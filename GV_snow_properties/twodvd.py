#!/usr/bin/env python

"""Structure and methods for 2D Video Disdrometer data
"""

#--------------------------------------------------------------------------
# Structure and methods for 2D Video Disdrometer data
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import collections
import numpy
import netCDF4

TwoDVDData = collections.namedtuple('TwoDVDData', ['times_utc',
                                                   'V_stats',
                                                   'S_eps_V_stats'])

def init(twodvd_fpath):
    f_ptr = netCDF4.Dataset(twodvd_fpath, 'r')

    times_utc = numpy.array(f_ptr['UTC_start'][:] + f_ptr['time_offset'][:], dtype='datetime64[s]')
    V_stats = f_ptr['V_stat_means'][:]
    S_eps_V_stats = f_ptr['S_eps_V_stats'][:]

    data = TwoDVDData(times_utc = times_utc,
                   V_stats = V_stats,
                   S_eps_V_stats = S_eps_V_stats)

    f_ptr.close()
    return data


if __name__ == "__main__":
    twodvd_fpath = "/boltzmann/data6/norm/GV_precip/GCPEx/Case_composites/05_min/CARE/twodvd_E02_CARE_05_minute_rematch.cdf"

    twodvd_data = init(twodvd_fpath)

    print twodvd_data.V_stats
    print twodvd_data.S_eps_V_stats
