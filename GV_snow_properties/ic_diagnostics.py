#!/usr/bin/env python

"""Information content diagnostic calculations
"""

#--------------------------------------------------------------------------
# Information content diagnostic calculations
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import numpy, numpy.linalg, math

def mat_power(S, p):
   evals, evecs = numpy.linalg.eig(S)
   U = evecs
   L = numpy.diag(evals)
   Uinv = numpy.linalg.inv(U)
   Lp = numpy.matrix(numpy.eye(S.shape[0]))
   for idx in range(S.shape[0]):
      Lp[idx,idx] = numpy.power(L[idx,idx],p)
   Sp = U*Lp*Uinv
   return Sp


def diagnostics(Sa, Sx, Sy, K):
   #Information Content diagnostics
   #Sa = the a priori state covariance matrix
   #Sx = the a posteriori state covariance matrix
   #Sy = the observation covariance matrix (actually the covariance matrix for
   #                                        the combined observation + forward model
   #                                        uncertainties)
   #K = the Jacobian of the forward model with respect to the state vector
   #See L'Ecuyer et al., 2006
   #First compute SIC
   det = numpy.linalg.det(Sa*numpy.linalg.pinv(Sx))
   #Base 2 SIC
   SIC = math.log(det,2)/2.
   #Base e SIC
   SIC_base_e = math.log(det)/2.


   #Next the degrees of freedom for signal
   #and the effective rank of the problem
   #(again, see L'Ecuyer et al., 2006)
   K_tilde = mat_power(Sy,-0.5)*K*mat_power(Sa,0.5)
   #Get the singular values for K_tilde
   U, s, V = numpy.linalg.svd(K_tilde)
   S_diag = numpy.diag(s)
   dof_signal = 0.
   eff_rank = 0
   alt_SIC = 0.
   for idx in range(s.shape[0]):
      dof_signal += s[idx]*s[idx]/(1. + s[idx]*s[idx])
      alt_SIC += math.log(1. + s[idx]*s[idx])
      if s[idx] > 1.:
         eff_rank += 1

   alt_SIC = alt_SIC/2.
   #print "Standard SIC, but base(e):", SIC_base_e, "Alternate SIC, base(e):", alt_SIC

   tmp_mat = mat_power(Sa, 0.5)
   R_mat = tmp_mat.I*Sx*tmp_mat.I
   I_mat = numpy.matrix(numpy.eye(5, dtype=float))
   det = numpy.linalg.det(R_mat)
   altalt_SIC = -1./2.*math.log(det,2)
   altalt_dof_signal = numpy.trace(I_mat - R_mat)
   #print altalt_SIC, altalt_dof_signal
   

   return (SIC, dof_signal, eff_rank)


