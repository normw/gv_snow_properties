#!/usr/bin/env python

"""Structure and methods for meteorology data
"""

#--------------------------------------------------------------------------
# Structure and methods for meteorology data
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import collections
import numpy
import netCDF4

_T_UNCERT = 0.5        #K,C
_P_UNCERT = 10.        #hPa

MetData = collections.namedtuple('MetData', ['times_utc',
                                             'T',
                                             's_T',
                                             'P',
                                             's_P',
                                             'u_2m'])

def near(x, y):
    atol = 1.e-08
    rtol = 1.e-05
    mask = numpy.less_equal(numpy.abs(x - y), atol + rtol*numpy.abs(y))
    return mask

def init(met_fpath):
    f_ptr = netCDF4.Dataset(met_fpath, 'r')

    times_utc = numpy.array(f_ptr['UTC_start'][:] + f_ptr['time_offset'][:], dtype='datetime64[s]')
    T = f_ptr['T'][:]
    T_valid = numpy.logical_not(near(T, f_ptr['T'].missing_value))
    T_K = numpy.ones_like(T)*f_ptr['T'].missing_value
    T_K[T_valid] = T[T_valid] + 273.15
    P = f_ptr['P'][:]
    u_2m = f_ptr['u_2m'][:]


    data = MetData(times_utc = times_utc,
                   T = T_K,
                   s_T = _T_UNCERT,
                   P = P,
                   s_P = _P_UNCERT,
                   u_2m = u_2m)

    return data


if __name__ == "__main__":
    met_fpath = "/boltzmann/data6/norm/GV_precip/GCPEx/Case_composites/05_min/CARE/met_tower_E02_CARE_05_minute.cdf"

    met_data = init(met_fpath)

    print met_data.T
    print met_data.P
