#!/usr/bin/env python

"""Compute size-resolved particle masses and horizontally-projected
areas based on particle sizes Dmax.

Particle mass may not exceed that of a sphere and area may not
exceed that of a circle with diameter Dmax.

These functions are formulated to accept either scalar or vector
arguments for the parameters alpha, beta, gamma, sigma.  If vector, 
they must be the same length as Dmax.

Dmax is assumed to be in meters, mass is returned in kilograms, and
area returned in square meters.  In the literature, alpha, beta,
gamma and sigma are usually defined based on centimeters and grams.
That convention is maintained here.
"""


#--------------------------------------------------------------------------
# Compute size-resolved particle masses and horizontally-projected areas
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------



import numpy

def mass_pow(Dmax, alpha, beta):
   """ Compute mass via a power law. """

   _RHO_ICE = 917.0  #kg m^-3

   # Allowing mass to be equal to zero is OK.
   _min_mass = 0.

   Dmax_cm = Dmax*100.
   mass_g = alpha*numpy.power(Dmax_cm, beta)
   mass = mass_g/1000.
   sphere_mass = 4./3.*numpy.pi*numpy.power(Dmax/2.,3.)*_RHO_ICE
   mass = numpy.clip(mass, _min_mass, sphere_mass)
   return mass

def area_pow(Dmax, gamma, sigma):
   """ Compute horizontally-projected area via a power law. """

   # Allowing area to be equal to zero causes problems in the 
   # fallspeed code.
   _min_area = 1.0e-50

   Dmax_cm = Dmax*100.
   area_cm2 = gamma*numpy.power(Dmax_cm, sigma)
   area = area_cm2/10000.
   circle_area = numpy.pi*numpy.power(Dmax,2.)/4.
   area = numpy.clip(area, _min_area, circle_area)
   return area


def test():
   Dmax = numpy.array([0.01, 0.1, 1.0, 10.0], dtype=float)*0.001
   alpha = [0.003, 0.006, 0.006, 0.009]
   beta = numpy.ones((4,), dtype=float)*1.9
   gamma = [0.3, 0.3, 0.3, 0.3]
   sigma = [2.2, 2.2, 2.2, 2.2]
   
   masses = mass_pow(Dmax, alpha, beta)
   A_ps = area_pow(Dmax, gamma, sigma)
   print masses
   print A_ps


if __name__ == "__main__":
    test()

