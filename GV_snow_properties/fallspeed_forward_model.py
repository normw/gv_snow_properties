#!/usr/bin/env python

"""Forward model for particle fallspeeds
"""

#--------------------------------------------------------------------------
# Forward model for particle fallspeeds
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import sys
import numpy
import pkg_resources
import particle_properties

correlation_matrix = None     #The matrix of error correlation values
correlation_D_mm   = None     #The D values associated with the error correlation values
I_max_D            = None     #The maximum index over which the D_values are defined
                              #(also defines the max indices for the correlation matrix)

delta0 = None
C0 = None
a0 = None
b0 = None

sigma_delta0 = None
sigma_C0 = None
sigma_a0 = None
sigma_b0 = None

XX_func = None

initialized = None

#30 June 2010
#Note, I've now disabled this and written a single function that covers both the
#MH 2005 and HW 2010 parameterizations.  The function accepts the delta0, C0, a0 and b0
#parameters as function arguments.  Although a single function is written, the
#calculation method for the Best number is different by a factor of sqrt(area ratio)
#Right now the Best number is calculated following Mitchell & Heymsfield.

#See notes from 27 May 2009 for verification of the calculations of
#rhof and anu in cgs units using P in hPa and T in K.

#These routines accept either vector or scalar arguments for the fallspeed
#parameters a_coeff, b_coeff, gamma_coeff and sigma_coeff.  If vector, they
#should be the same length as dar.

def XX_MH2005(m_ov_a, D_cm, grav, rhof, anu, Ar):
   #Ar is included in the argument list so that the calls to XX_MH2005 and XX_HW2010 match
   #but Ar is not used in this calc
   XX = 2.*m_ov_a*numpy.power(D_cm,2)*grav/(rhof*numpy.power(anu,2))
   return XX

def XX_HW2010(m_ov_a, D_cm, grav, rhof, anu, Ar):
   XX = 2.*m_ov_a*numpy.power(D_cm,2)*grav/(rhof*numpy.power(anu,2))*numpy.sqrt(Ar)
   return XX



def init(f_name, model_type = None):
   import string
   #Read the correlation matrix and define the fallspeed model
   global correlation_matrix, correlation_D_mm, I_max_D, delta0, C0, a0, b0, sigma_delta0, sigma_C0, sigma_a0, sigma_b0, XX_func, initialized

   if model_type == 'MH2005':
      f_in = pkg_resources.resource_stream(__name__, 'data/correlation_matrix_MH2005_5x5.txt')
   elif model_type == 'HW2010':
      f_in = pkg_resources.resource_stream(__name__, 'data/correlation_matrix_HW2010_5x5.txt')
   else:
      sys.stderr.write('In fallspeed_forward_model.init(), model_type must be MH2005 or HW2010\n')
      sys.exit(1)

   lines = f_in.readlines()
   f_in.close()
   #The first line contains the number of size bins
   N_bins = int(lines[0])

   I_max_D = N_bins - 1
   #The second line contains the sizes
   #Declare the arrays
   correlation_D_mm = numpy.zeros((N_bins,), dtype=float)
   correlation_matrix = numpy.asmatrix(numpy.zeros((N_bins, N_bins), dtype=float))

   #Read the sizes
   correlation_D_mm[:] = map(float,string.splitfields(lines[1]))
   #Read the correlation matrix
   for i_bin, line in enumerate(lines[2:]):
      correlation_matrix[i_bin,:] = map(float, string.splitfields(line))
   #Done

   #Set the model type
   set_model(model_type)

   initialized = True

def set_model(model_type = None):
   global delta0, C0, a0, b0, sigma_delta0, sigma_C0, sigma_a0, sigma_b0, XX_func
   
   if model_type == 'HW2010':
      #Heymsfield Westbrook 2010
      delta0 = 8.00
      C0 = 0.35
      a0 = 0.
      b0 = 0.
      
      #My estimates based on differences between MH2005 and HW2010
      sigma_delta0 = 2.17
      sigma_C0 = 0.25
      sigma_a0 = 0.0
      sigma_b0 = 0.0

      XX_func = XX_HW2010

   elif model_type == 'MH2005':
      #Mitchell Heymsfield 2005
      delta0 = 5.83
      C0 = 0.6
      a0 = 0.0017
      b0 = 0.8

      #My estimates based on differences between MH2005 and HW2010
      sigma_delta0 = 2.17
      sigma_C0 = 0.25
      sigma_a0 = 0.0
      sigma_b0 = 0.0

      XX_func = XX_MH2005

   else:
      #Default to Mitchell & Heymsfield 2005
      #Mitchell Heymsfield 2005
      delta0 = 5.83
      C0 = 0.6
      a0 = 0.0017
      b0 = 0.8

      #My estimates based on differences between MH2005 and HW2010
      sigma_delta0 = 2.17
      sigma_C0 = 0.25
      sigma_a0 = 0.0
      sigma_b0 = 0.0

      XX_func = XX_MH2005
      #Done  
      
def get_parameters():
   global delta0, C0, a0, b0
   return (delta0, C0, a0, b0)

def get_parameter_uncerts():
   global sigma_delta0, sigma_C0, sigma_a0, sigma_b0
   return(sigma_delta0, sigma_C0, sigma_a0, sigma_b0)


def fallspeed(D_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, delta0, C0, a0, b0, fractional_vtx_error=None):
   global XX_func
   #This is a generic fallspeed function that covers both the Mitchell & Heymsfield 2005 
   #and the Heymsfield and Westbrook 2010 parameterizations by various choices of delta0, C0, a0 and b0
   #However, the calculation of the Best number is slightly different between the two so this has to be
   #set manually in the code for now
   #Note that this routine accepts D in mm rather than cm, so that I don't have to do conversions
   #when calculating Kb*Sb*Kb^T
   #a_coeff and b_coeff are the mass-dimension power law coefficient and exponent
   #gamma_coeff and sigma_coeff are the area-dimension power law coefficient and exponent
   #T_K is temperature in K, scalar
   #P_hPa is pressure in hPa, scalar

   grav=980.

   #Convert D to cm
   D_cm = D_mm*0.1

   rhof = 0.348e-3 * P_hPa/T_K
   
   mass = particle_properties.mass_pow(D_cm*0.01, a_coeff, b_coeff)            #kg
   #(area, area_circle) = particle_properties.area_pow(D_cm*0.01, gamma_coeff, sigma_coeff)    #cm^2
   area = particle_properties.area_pow(D_cm*0.01, gamma_coeff, sigma_coeff)    #cm^2

   #Have to handle some locations where mass, area are zero because D_mm = 0.
   indices_bad = numpy.nonzero(D_mm == 0.)
   #This gives an 'invalid' fp error where mass=0 and area = 0
   numpy.seterr(divide='ignore', invalid='ignore')
   m_ov_a = mass*1000./(area*10000.)    #Need g/cm^2
   area_circle = numpy.pi*numpy.power(D_cm,2)/4.
   Ar = area/area_circle
   numpy.seterr(divide='raise', invalid='raise')
   m_ov_a[indices_bad] = 0.
   Ar[indices_bad] = 0.

   try:
       anu = 0.043/P_hPa * (numpy.power(T_K,2.5))/(T_K+120.)
   except FloatingPointError:
       print T_K
       assert 0
   XX = XX_func(m_ov_a, D_cm, grav, rhof, anu, Ar)

   k1 = numpy.power(delta0,2)/4.
   term = 1. + 4.*numpy.sqrt(XX)/(numpy.power(delta0,2)*numpy.sqrt(C0))
   try:
      reynolds = k1*numpy.power(numpy.sqrt(term) - 1., 2) - a0*numpy.power(XX,b0)
   except:
      print "Invalid value in subtract, line 174 in fallspeed_forward_model"
      print term
      print XX
      print area
      #If areas are zero (from trying to make gamma negative), XX goes to inf
      #and the first term in the reynolds expression goes to inf
   #This will also raise a fp error where D_cm = 0 and reynolds = 0
   numpy.seterr(divide='ignore', invalid='ignore')
   vtx=anu/D_cm*reynolds
   numpy.seterr(divide='raise', invalid='raise')
   vtx[indices_bad] = 0.
   vtx = vtx/100.  #Convert to m/s

   #If fractional_vtx_error is greater than 0., compute the covariance matrix and return it also
   if fractional_vtx_error == None:
      return vtx
   else:
      global correlation_matrix, correlation_D_mm, I_max_D
      N_bins = len(D_mm)
      covariance_matrix = numpy.asmatrix(numpy.zeros((N_bins, N_bins), dtype=float))

      #Create a matrix of vtx[i]*vtx[j], will be N_bins x N_bins
      vtx_vector = numpy.asmatrix(vtx)
      vtx2_matrix = vtx_vector.T*vtx_vector

      #Now need to create a correlation matrix matched to these sizes D_mm
      correlation_matrix_local = numpy.asmatrix(numpy.zeros((N_bins, N_bins), dtype=float))
      #Map the D_vals to correlation_D
      i_map = []
      for i_bin in range(N_bins):
         delta = numpy.abs(D_mm[i_bin] - correlation_D_mm)
         i_match = numpy.argmin(delta)
         if i_match < I_max_D:
            i_map.append(i_match)
         else:
            i_map.append(-1)

      #Now fill the correlation matrix
      for i_x in range(N_bins):
         for i_y in range(i_x, N_bins):
            if i_map[i_x] >= 0 and i_map[i_y] >= 0:
               correlation_val = correlation_matrix[i_map[i_x], i_map[i_y]]
            elif i_x == i_y:
               correlation_val = 1.0
            else:
               correlation_val = 0.
            correlation_matrix_local[i_x, i_y] = correlation_val
            correlation_matrix_local[i_y, i_x] = correlation_val

      #Now compute the covariance matrix
      #I need element-by-element multiplication of vtx^2 and the correlation matrix
      covariance_matrix[:,:] = fractional_vtx_error*fractional_vtx_error*numpy.multiply(vtx2_matrix,correlation_matrix_local)
      return(vtx, covariance_matrix)

   
   
if __name__ == '__main__':
   import sys

   def mytest1(a_coeff, b_coeff, gamma_coeff, sigma_coeff):
      #Initialize the fallspeed model
      init('correlation_matrix_MH2005_5x5.txt', 'MH2005')
      #Get the parameters
      (delta0, C0, a0, b0) = get_parameters()

      T_K = 263.
      P_hPa = 1005.
      D_major_m = numpy.array(range(1,1000,1), dtype=float)/100000.
      D_obs_m = D_major_m
      D_obs_mm = D_obs_m*1000.
      vtx_MH2005 = fallspeed(D_obs_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, delta0, C0, a0, b0)
      #Now also get the covariance matrix
      (vtx_MH2005, vtx_cov_matrix) = fallspeed(D_obs_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, delta0, C0, a0, b0, fractional_vtx_error=0.30)
      #Reset the model
      set_model("HW2010")
      vtx_HW2010 = fallspeed(D_obs_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, delta0, C0, a0, b0)

    
      #Let's also get the area ratios
      masses = particle_properties.mass_pow(D_major_m, a_coeff, b_coeff)
      areas =  particle_properties.area_pow(D_major_m, gamma_coeff, sigma_coeff)
      areas_circle = numpy.pi*numpy.power(D_major_mm*0.01, 2)/4.
      Ar = areas/areas_circle

      print "#  a = %15.5e, b = %15.5e, gamma =  %15.5e, sigma = %15.5e" %(a_coeff, b_coeff, gamma_coeff, sigma_coeff)
      print "#      D_major_mm          Area ratio       MH_2005         HW_2010   vtx_cov_matrix[i,i] for MH2005"
      for i in range(1,len(D_major_m)):
         print "%15.5e %15.5e %15.5e %15.5e %15.5e" %(D_obs_mm[i], Ar[i], vtx_MH2005[i], vtx_HW2010[i], numpy.sqrt(vtx_cov_matrix[i,i]))


   def mytest2(a_coeff, b_coeff, gamma_coeff, sigma_coeff):
      #Initialize the fallspeed model
      init('correlation_matrix_MH2005_5x5.txt', 'MH2005')
      #Get the parameters
      (delta0, C0, a0, b0) = get_parameters()

      T_K = 230.
      P_hPa = 220. 
      RHO_ICE = 917.  #kg m^-3

      D_major_m = numpy.array(range(1,1000,1), dtype=float)/100000.
      D_obs_m = D_major_m
      D_obs_mm = D_obs_m*1000.
      vtx_MH2005 = fallspeed(D_obs_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, delta0, C0, a0, b0)
    
      #Let's also get the area ratios
      masses = particle_properties.mass_pow(D_major_m, a_coeff, b_coeff)

      #Get the solid ice sphere diameter
      D_sphere_mm = numpy.power((masses/RHO_ICE*6/numpy.pi), 1./3.)*1000.

      areas =  particle_properties.area_pow(D_major_m, gamma_coeff, sigma_coeff)
      areas_circle = numpy.pi*numpy.power(D_major_m*0.01, 2)/4.
      Ar = areas/areas_circle

      print "#  a = %15.5e, b = %15.5e, gamma =  %15.5e, sigma = %15.5e" %(a_coeff, b_coeff, gamma_coeff, sigma_coeff)
      print "#      D_major_mm          D_sphere_mm     Area ratio       vtx,MH_2005, m/s"
      for i in range(1,len(D_major_m)):
         print "%15.5e %15.5e %15.5e %15.5e" %(D_obs_mm[i], D_sphere_mm[i], Ar[i], vtx_MH2005[i])




   #a_coeff = 0.0028
   #b_coeff = 2.1
   #gamma_coeff = 0.2285
   #sigma_coeff = 1.88
   #mytest1(a_coeff, b_coeff, gamma_coeff, sigma_coeff)

   a_coeff = 0.00739
   b_coeff = 2.45
   gamma_coeff = 0.2285
   sigma_coeff = 1.88
   mytest2(a_coeff, b_coeff, gamma_coeff, sigma_coeff)
