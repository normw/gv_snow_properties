#!/usr/bin/env python


"""Snowfall rate forward model
"""

#--------------------------------------------------------------------------
# Snowfall rate forward model
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import fallspeed_forward_model
import numpy
import particle_properties

#Now compute mass inside the snowfall rate subroutine

def snowfall_rate(N_D_mm, D_major_mm, widths_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, delta0, C0, a0, b0, T_K, P_hPa, vtx_covar_matrix, D_vtx_stats_mm):
   #Compute snowfall rate and the Jacobian of the snowfall rate
   #Get Vtx at size bins for precip calc
   vtx_ms =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                               a_coeff,          \
                                               b_coeff,          \
                                               gamma_coeff,      \
                                               sigma_coeff,      \
                                               T_K,              \
                                               P_hPa,            \
                                               delta0,           \
                                               C0,               \
                                               a0,               \
                                               b0)

   masses_kg = particle_properties.mass_pow(D_major_mm*0.001, a_coeff, b_coeff)


   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm*vtx_ms*widths_mm
   snowfall_mass_flux_base = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_base = snowfall_mass_flux_base*3600.

   #Compute the snowfall rate variance and the snowfall rate covariances wrt
   #the V_stats due to the Vtx uncertainties
   #Make a vector containing the snowfall rate Jacobian wrt to fallspeeds
   dS_dvtx = numpy.asmatrix(masses_kg*N_D_mm*widths_mm, dtype=float)*3600.
   #Bin widths are half at the first and last interval
   dS_dvtx[0] = dS_dvtx[0]/2.
   dS_dvtx[-1] = dS_dvtx[-1]/2.
   #Make vectors for the derivatives dV0/dv, d(V1)/dv and d(V2)/dv
   N_bins = len(D_major_mm)
   dV0_dvtx = numpy.asmatrix(numpy.zeros((N_bins,), dtype=float))
   dV1_dvtx = numpy.asmatrix(numpy.zeros((N_bins,), dtype=float))
   dV2_dvtx = numpy.asmatrix(numpy.zeros((N_bins,), dtype=float))
   #Find the indices of D_major_mm nearest to D_vtx_stats_mm
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[0])
   i_D0 = numpy.argmin(deltas)
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[1])
   i_D1 = numpy.argmin(deltas)
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[2])
   i_D2 = numpy.argmin(deltas)
   dV0_dvtx[0,i_D0] = 1.
   dV1_dvtx[0,i_D1] = 1.
   dV2_dvtx[0,i_D2] = 1.

   d_DeltaV1_dvtx = dV0_dvtx - dV1_dvtx
   d_DeltaV2_dvtx = dV0_dvtx - dV2_dvtx

   #Then the variance is just vector*covariance_matrix*vector_transpose
   var_S_vtx = dS_dvtx*vtx_covar_matrix*dS_dvtx.T
   covar_S_V0 = dS_dvtx*vtx_covar_matrix*dV0_dvtx.T
   covar_S_DeltaV1 = dS_dvtx*vtx_covar_matrix*d_DeltaV1_dvtx.T
   covar_S_DeltaV2 = dS_dvtx*vtx_covar_matrix*d_DeltaV2_dvtx.T

   S_Vstats_covar = (var_S_vtx, covar_S_V0, covar_S_DeltaV1, covar_S_DeltaV2)

   #Do the jacobian
   dS_dB = numpy.zeros((2*N_bins+6), dtype=float)
   #First perturb D
   D_major_mm_pert = numpy.copy(D_major_mm)
   for i_bin in range(N_bins):
      i_jac = i_bin
      #D_major_mm[0] = 0, work around
      delta_D = numpy.max([D_major_mm[i_bin]*0.01, 0.25*0.01])
      D_major_mm_pert[i_bin] = D_major_mm[i_bin]+delta_D
      masses_kg_pert = particle_properties.mass_pow(D_major_mm_pert*0.001, a_coeff, b_coeff)
      vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm_pert,  \
                                                       a_coeff,          \
                                                       b_coeff,          \
                                                       gamma_coeff,      \
                                                       sigma_coeff,      \
                                                       T_K,              \
                                                       P_hPa,            \
                                                       delta0,           \
                                                       C0,               \
                                                       a0,               \
                                                       b0)
      
      snowfall_mass_flux_integral_terms = masses_kg_pert*N_D_mm*vtx_ms_pert*widths_mm
      snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
      S_pert = snowfall_mass_flux_pert*3600.
      dS_dB[i_jac] = (S_pert - S_base)/delta_D
      #Now reset the perturbation
      D_major_mm_pert[i_bin] = D_major_mm[i_bin]

   #Next perturb N_D
   N_D_mm_pert = numpy.copy(N_D_mm)
   for i_bin in range(N_bins):
      i_jac = i_bin+N_bins
      #What if N_D is zero?
      delta_ND = numpy.max([N_D_mm[i_bin]*0.01, 1.0*0.01])
      N_D_mm_pert[i_bin] = N_D_mm[i_bin]+delta_ND
      snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms*widths_mm
      snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
      S_pert = snowfall_mass_flux_pert*3600.
      dS_dB[i_jac] = (S_pert - S_base)/delta_ND
      #Now reset the perturbation
      N_D_mm_pert[i_bin] = N_D_mm[i_bin]

   #Now perturb the other parameters
   #First the temperature
   #T_K will never be zero
   delta_T_K = T_K*0.01
   T_K_pert = T_K + delta_T_K
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K_pert,         \
                                                    P_hPa,            \
                                                    delta0,           \
                                                    C0,               \
                                                    a0,               \
                                                    b0)
   i_jac = 2*N_bins
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_T_K


   #Next the pressure
   #P will never be zero
   delta_P_hPa = P_hPa*0.01
   P_hPa_pert = T_K + delta_P_hPa
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K,              \
                                                    P_hPa_pert,       \
                                                    delta0,           \
                                                    C0,               \
                                                    a0,               \
                                                    b0)
   i_jac = 2*N_bins+1
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_P_hPa

   #Next delta0
   #delta0 will never be zero
   delta_delta0 = delta0*0.01
   delta0_pert = delta0 + delta_delta0
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K,              \
                                                    P_hPa,            \
                                                    delta0_pert,      \
                                                    C0,               \
                                                    a0,               \
                                                    b0)
   i_jac = 2*N_bins+2
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_delta0
      
   #Next C0
   #C0 will never be zero
   delta_C0 = C0*0.01
   C0_pert = C0 + delta_C0
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K,              \
                                                    P_hPa,            \
                                                    delta0,           \
                                                    C0_pert,          \
                                                    a0,               \
                                                    b0)
   i_jac = 2*N_bins+3
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_C0


   #Next a0
   #a0 may be zero sometimes
   delta_a0 = numpy.max([a0*0.01, 0.0017*0.01])
   a0_pert = a0 + delta_a0
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K,              \
                                                    P_hPa,            \
                                                    delta0,           \
                                                    C0,               \
                                                    a0_pert,          \
                                                    b0)
   i_jac = 2*N_bins+4
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_a0


   #Finally b0
   #b0 may be zero sometimes      
   delta_b0 = numpy.max([b0*0.01, 0.8*0.01])
   b0_pert = b0 + delta_b0
   vtx_ms_pert =  fallspeed_forward_model.fallspeed(D_major_mm,       \
                                                    a_coeff,          \
                                                    b_coeff,          \
                                                    gamma_coeff,      \
                                                    sigma_coeff,      \
                                                    T_K,              \
                                                    P_hPa,            \
                                                    delta0,           \
                                                    C0,               \
                                                    a0,               \
                                                    b0_pert)
   i_jac = 2*N_bins+5
   snowfall_mass_flux_integral_terms = masses_kg*N_D_mm_pert*vtx_ms_pert*widths_mm
   snowfall_mass_flux_pert = 0.5*snowfall_mass_flux_integral_terms[0] + numpy.sum(snowfall_mass_flux_integral_terms[1:-1]) + 0.5*snowfall_mass_flux_integral_terms[-1]
   S_pert = snowfall_mass_flux_pert*3600.
   dS_dB[i_jac] = (S_pert - S_base)/delta_b0

   #Jacobian is done

      
   return (S_base, dS_dB, S_Vstats_covar, dS_dvtx)


if __name__ == "__main__":
   def mytest():
      import particle_properties
      a_coeff = 0.003
      b_coeff = 1.9
      gamma_coeff = 0.35
      sigma_coeff = 1.7
      N0_mm = 1.0e4
      lambda_mm = 1.0
      T_K = 263.
      P_hPa = 1000.

      #Get masses, D and size distribution
      D_major_mm = numpy.arange(0, 105, 1, dtype=float)*0.25
      widths_mm=  numpy.ones((105,), dtype=float)*0.25
      masses_kg = particle_properties.mass_pow(D_major_mm*0.001, a_coeff, b_coeff)
      N_D = N0_mm*numpy.exp(-lambda_mm*D_major_mm)

      (delta0, C0, a0, b0) = fallspeed_forward_model.get_parameters()
      (S, dS_dB) = snowfall_rate(masses_kg, N_D, D_major_mm, widths_mm, a_coeff, b_coeff, gamma_coeff, sigma_coeff, delta0, C0, a0, b0, T_K, P_hPa)
      print S
      N_bins = len(D_major_mm)
      for i_bin in range(N_bins):
         i_jac = i_bin
         print "%4d %15.5e %15.5e" %(i_jac, D_major_mm[i_bin], dS_dB[i_jac])
      print " "
      for i_bin in range(N_bins):
         i_jac = N_bins+i_bin
         print "%4d %15.5e %15.5e" %(i_bin, N_D[i_bin], dS_dB[i_jac])
      print " "
      parms = [T_K, P_hPa, delta0, C0, a0, b0]
      for i_bin in range(6):
         i_jac = 2*N_bins + i_bin
         print "%4d %15.5e %15.5e" %(i_bin, parms[i_bin], dS_dB[i_jac])
    

   mytest()
