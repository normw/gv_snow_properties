#!/usr/bin/env python

"""Prepare data for input into the GV_snow_properties retrieval
"""

#--------------------------------------------------------------------------
# Prepare data for input into the GV_snow_properties retrieval
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------



import netCDF4
import numpy
import glob
import os


def prep_gcpex(event, location, mrr_instrument, pip_instrument, data_dir):

    MRR_PATTERN = "mrr_%s*_*%s*.cdf"
    PIP_PATTERN = "pip_%s*_*%s*.cdf"
    TWODVD_PATTERN = "twodvd_%s*_*%s*.cdf"
    PRECIP_PATTERN = "precipitation_%s*_*%s*.cdf"
    MET_PATTERN = "met_tower_%s*_*%s*.cdf"
    
    #For PIP
    if pip_instrument is not None:
        pip_stream = "%s_%s" %(location, pip_instrument)
    else:
        pip_stream = "%s" %(location,)
    
    
    #For MRR
    if mrr_instrument is not None:
        mrr_stream = "%s_%s" %(location, mrr_instrument)
    else:
        mrr_stream = "%s" %(location,)
    
    #For other instruments
    stream = "%s" %(location,)
    
    mrr_files = glob.glob(os.path.join(data_dir, location, MRR_PATTERN %(event, mrr_stream)))
    pip_files = glob.glob(os.path.join(data_dir, location, PIP_PATTERN %(event, pip_stream)))
    twodvd_files = glob.glob(os.path.join(data_dir, location, TWODVD_PATTERN %(event, stream)))
    precip_files = glob.glob(os.path.join(data_dir, location, PRECIP_PATTERN %(event, stream)))
    met_files = glob.glob(os.path.join(data_dir, location, MET_PATTERN %(event, stream)))

    assert len(mrr_files) == 1
    assert len(pip_files) == 1
    assert len(twodvd_files) == 1
    assert len(precip_files) == 1
    assert len(met_files) == 1

    #Verify time matching
    mrr_ptr = netCDF4.Dataset(mrr_files[0], 'r')
    pip_ptr = netCDF4.Dataset(pip_files[0], 'r')
    twodvd_ptr = netCDF4.Dataset(twodvd_files[0], 'r')
    precip_ptr = netCDF4.Dataset(precip_files[0], 'r')
    met_ptr = netCDF4.Dataset(met_files[0], 'r')

    mrr_times = numpy.array(mrr_ptr['UTC_start'][:] + mrr_ptr['time_offset'][:], dtype='datetime64[s]')
    pip_times = numpy.array(pip_ptr['UTC_start'][:] + pip_ptr['time_offset'][:], dtype='datetime64[s]')
    twodvd_times = numpy.array(twodvd_ptr['UTC_start'][:] + twodvd_ptr['time_offset'][:], dtype='datetime64[s]')
    precip_times = numpy.array(precip_ptr['UTC_start'][:] + precip_ptr['time_offset'][:], dtype='datetime64[s]')
    met_times = numpy.array(met_ptr['UTC_start'][:] + met_ptr['time_offset'][:], dtype='datetime64[s]')
   
    #Verify start times agree 
    assert mrr_times[0] == pip_times[0] == twodvd_times[0] == precip_times[0] == met_times[0]

    #Check the shortest time range
    N_times = numpy.amin([mrr_times.shape[0],
                          pip_times.shape[0],
                          twodvd_times.shape[0],
                          precip_times.shape[0],
                          met_times.shape[0]])


    #Check that all times in range N_times agree
    for i_time in range(1, N_times):
        assert mrr_times[i_time] == pip_times[i_time] == twodvd_times[i_time] == precip_times[i_time] == met_times[i_time]

    mrr_ptr.close()
    pip_ptr.close()
    twodvd_ptr.close()
    precip_ptr.close()
    met_ptr.close()
   
    return (mrr_files[0], pip_files[0], twodvd_files[0], precip_files[0], met_files[0], N_times)

if __name__ == "__main__":
    import ret_config
    data_dir = ret_config.config.get('Main', 'data_dir')
    event = ret_config.config.get('Case', 'event')
    location = ret_config.config.get('Case', 'location')
    mrr_instrument = ret_config.config.get('Case', 'MRR_instrument')
    pip_instrument = ret_config.config.get('Case', 'PIP_instrument')


    results = prep_gcpex(event, location, mrr_instrument, pip_instrument, data_dir)
