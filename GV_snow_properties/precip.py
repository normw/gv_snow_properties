#!/usr/bin/env python

"""Structure and methods for precipitation data
"""

#--------------------------------------------------------------------------
# Structure and methods precipitation data
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------




import collections
import numpy
import netCDF4

#Pluvio 400s were unheated, deployed inside the double fence,
#and fitted with an Alter shield

#Pluvio 200s were heated, deployed outside the double fence,
#and fitted with both Alter and Tretyakov shields.

#GCPEx was windy.  Rely primarily on the P400, unless it says:
#   Missing:  Then check the P200
#   No snow:  Then check the P200
#
# Then if the P200 has a value, use it.

PrecipData = collections.namedtuple('PrecipData', ['times_utc',
                                                   'dt_sampling',
                                                   'valid',
                                                   'rate',
                                                   's_rate'])

_MISSING_FLOAT = -999.

def co_process(accum_A, accum_A_missing_value,
               accum_B, accum_B_missing_value):

    N_obs = accum_A.shape[0]
    accum = numpy.zeros((N_obs,), dtype=float)
    valid = numpy.zeros((N_obs,), dtype=bool)
    valid[:] = False

    for i_obs in range(N_obs):
        if near(accum_A[i_obs], accum_A_missing_value):
            #accum_A is missing, check accum_B
            if near(accum_B[i_obs], accum_B_missing_value):
                accum[i_obs] = accum_A_missing_value
                valid[i_obs] = False
            else:
                accum[i_obs] = accum_B[i_obs]
                valid[i_obs] = True
        elif near(accum_A[i_obs], 0.):
            if accum_B[i_obs] > 0.:
                accum[i_obs] = accum_B[i_obs]
                valid[i_obs] = True
            else:
                accum[i_obs] = accum_A[i_obs]
                valid[i_obs] = True
        else:
            accum[i_obs] = accum_A[i_obs]
            valid[i_obs] = True

        #Catch any NaNs in the accum array and mark not valid
        mask = numpy.isnan(accum)
        valid[mask] = False

    return accum, valid
            
def eval_rate_uncerts(rates):
    #Rate in mm/h
    #Assumes only valid (non-missing) rates are passed in
    uncerts = 0.3*rates
    uncerts[rates < 0.5] = 0.5*rates[rates < 0.5]
    uncerts[rates < 0.05] = 0.03
    uncerts[rates <= 0.] = 0.
    return uncerts

def near(x, y):
    atol = 1.e-08
    rtol = 1.e-05
    mask = numpy.less_equal(numpy.abs(x - y), atol + rtol*numpy.abs(y))
    return mask

def init(precip_fpath):

    f_ptr = netCDF4.Dataset(precip_fpath, 'r')

    times_utc = numpy.array(f_ptr['UTC_start'][:] + f_ptr['time_offset'][:], dtype='datetime64[s]')

    accum_p400 = None
    accum_p200 = None
    try:
        accum_p400 = f_ptr['pluvio_400_accums'][:]
        accum_p400_missing_value = f_ptr['pluvio_400_accums'].missing_value
    except IndexError:
        pass

    try:
        accum_p200 = f_ptr['pluvio_200_accums'][:]
        accum_p200_missing_value = f_ptr['pluvio_200_accums'].missing_value
    except IndexError:
        pass

    if accum_p400 is not None:
        if accum_p200 is not None:
            accum, valid = co_process(accum_p400, accum_p400_missing_value,
                                      accum_p200, accum_p200_missing_value)
    
        else:
            accum[:] = accum_p400[:]    
            valid = numpy.logical_and(numpy.logical_not(near(accum, accum_p400_missing_value)),
                                     numpy.logical_not(numpy.equal(accum, numpy.nan)))
    else:
        #accum_p400 is None
        if accum_p200 is not None:
            print "E"
            accum[:] = accum_p200[:]    
            valid = numpy.logical_and(numpy.logical_not(near(accum, accum_p400_missing_value)),
                                     numpy.logical_not(numpy.equal(accum, numpy.nan)))
        else:
            accum[:] = _MISSING_FLOAT   
            valid[:] = False

    rates = numpy.ones_like(accum)*(_MISSING_FLOAT)
    rates_uncerts = numpy.ones_like(accum)*(_MISSING_FLOAT)
    rates[valid] = accum[valid]/(300./3600.)
    rates_uncerts[valid] = eval_rate_uncerts(rates[valid])
    
    data = PrecipData(times_utc = times_utc,
                      dt_sampling = 300.,
                      valid = valid,
                      rate = rates,
                      s_rate = rates_uncerts)

    return data




if __name__ == '__main__':
    precip_fpath = '/boltzmann/data6/norm/GV_precip/GCPEx/Case_composites/05_min/CARE/precipitation_E02_CARE_05_minute.cdf'

    data = init(precip_fpath)
    print data.dt_sampling
    print data.valid
    print data.accumulation
