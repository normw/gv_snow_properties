#!/usr/bin/env python

"""Full forward model for optimal estimation
"""

#--------------------------------------------------------------------------
# Full forward model for optimal estimation
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import numpy, particle_properties, numpy.linalg
import rayleigh_sim_w_Kb as rayleigh_sim
import snowfall_rate_forward_model
import fallspeed_forward_model
import vstat_forward_model

#These are the sizes at which the fallpseed stats are calculated
D_vtx_stats_mm = numpy.array((4.0, 2.0, 1.0), dtype=float)
widths_vtx_stats_mm = numpy.array((0.5, 0.25, 0.25), dtype=float)

#Initialize the fallspeed forward model
fallspeed_forward_model.init('correlation_matrix_MH2005_5x5.txt', 'MH2005')
#fallspeed_forward_model.init('correlation_matrix_HW2010_5x5.txt', 'HW2010')

def fmodel1(Xi,           \
             N_D_mm,       \
             N_D_uncert_mm, \
             D_obs_mm,     \
             D_obs_uncert_mm, \
             widths_obs_mm,\
             T_K,          \
             T_uncert_K,   \
             P_hPa,        \
             P_uncert_hPa, \
             freq,         \
             K_sq_ice,     \
             K_sq_ice_uncert, \
             K_sq_water,   \
             K_sq_water_uncert, \
             diagnostics = False, \
             dump_params_uncerts = False):

   #The forward model.  Given the mass and area parameters, computes the X-band dBZe,
   #the precip rate, and the rms difference between observed and modeled fallspeeds.
   Y_f = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
   S_f = numpy.asmatrix(numpy.zeros((5,5),dtype=float))

   #a_coeff = Xi[0,0]
   a_coeff = numpy.exp(Xi[0,0])
   b_coeff = Xi[1,0]
   #gamma_coeff = Xi[2,0]
   gamma_coeff = numpy.exp(Xi[2,0])
   sigma_coeff = Xi[3,0]
   f_coeff = Xi[4,0]

   #Rescale D_obs to estimated D_major, and rescale the size distribution appropriately
   D_major_mm = D_obs_mm/f_coeff
   D_major_uncert_mm = D_obs_uncert_mm/f_coeff
   widths_mm = widths_obs_mm/f_coeff
   N_D_mm_mod = numpy.copy(N_D_mm)
   N_D_mm_mod = N_D_mm_mod*f_coeff
   N_D_uncert_mm_mod = numpy.copy(N_D_uncert_mm)
   N_D_uncert_mm_mod = N_D_uncert_mm_mod*f_coeff
   
   #Get the fallspeed parameters and their uncertainty estimates
   (delta0, C0, a0, b0) = fallspeed_forward_model.get_parameters()
   (uncert_delta0, uncert_C0, uncert_a0, uncert_b0) = fallspeed_forward_model.get_parameter_uncerts()

   #Get Vtx at size bins (needed for reflectivity weighted fallspeed)
   (est_vtx_ms, vtx_covar_matrix) =  fallspeed_forward_model.fallspeed(D_major_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0,               \
                                                                       fractional_vtx_error = 0.30)
                                                                       #fractional_vtx_error = 0.10)


   #Get the radar forward model results and its Jacobian wrt parameters B
   (est_VertiX_dBZe, est_VertiX_Vdop, ddBZe_dB) = rayleigh_sim.rayleigh_sim_spectrum2(N_D_mm_mod, \
                                                                                      D_major_mm, \
                                                                                      widths_mm,  \
                                                                                      a_coeff,    \
                                                                                      b_coeff,    \
                                                                                      est_vtx_ms, \
                                                                                      K_sq_water, \
                                                                                      K_sq_ice)


   #Get the V_stats forward model results and its Jacobian wrt parameters B
   (est_vtx_aves, S_f_V_stats_model, dV_stats_dB, dV_stats_dvtx) = vstat_forward_model.V_stats(a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, D_major_mm)

   #Get the snowfall rate; its jacobian with respect to parameters, b, Kb; and its variance due to fallspeed uncertainties
   (est_precip_rate, dP_db, P_Vstats_covar_vtx, dP_dvtx) = snowfall_rate_forward_model.snowfall_rate(N_D_mm_mod,     \
                                                                                                     D_major_mm,     \
                                                                                                     widths_mm,      \
                                                                                                     a_coeff,        \
                                                                                                     b_coeff,        \
                                                                                                     gamma_coeff,    \
                                                                                                     sigma_coeff,    \
                                                                                                     delta0,         \
                                                                                                     C0,             \
                                                                                                     a0,             \
                                                                                                     b0,             \
                                                                                                     T_K,            \
                                                                                                     P_hPa,          \
                                                                                                     vtx_covar_matrix, \
                                                                                                     D_vtx_stats_mm)
   N_bins = len(D_major_mm)

   #Uncertainties due to Vtx uncertainties
   dX_dvtx = numpy.asmatrix(numpy.zeros((5, N_bins), dtype=float))
   dX_dvtx[1,:] = dP_dvtx[:]
   dX_dvtx[2:,:] = dV_stats_dvtx[:,:]

   S_f_vtx = dX_dvtx*vtx_covar_matrix*dX_dvtx.T


   #Uncertainties due to parameter sensitivities (ie. KbSbKbT)
   #Need to do one grand evaluation to get covariances
   #First build the Sb matrix, the covariance matrix of the parameters, b_tilde.
   #  It has size (2*N_bins + 8) x (2*N_bins + 8)
   Sb = numpy.asmatrix(numpy.zeros((2*N_bins + 8, 2*N_bins+8), dtype=float))
   #Build four submatrices, then load them into Sb
   variance_vals = D_major_uncert_mm*D_major_uncert_mm
   Sb_block1 = numpy.asmatrix(numpy.diag(variance_vals), dtype=float)
   variance_vals = N_D_uncert_mm_mod*N_D_uncert_mm_mod
   Sb_block2 = numpy.asmatrix(numpy.diag(variance_vals), dtype=float)
   variance_vals = [K_sq_ice_uncert*K_sq_ice_uncert, K_sq_water_uncert*K_sq_water_uncert]
   Sb_block3 = numpy.asmatrix(numpy.diag(variance_vals), dtype=float)
   variance_vals = [T_uncert_K*T_uncert_K, P_uncert_hPa*P_uncert_hPa, uncert_delta0*uncert_delta0, uncert_C0*uncert_C0, uncert_a0*uncert_a0, uncert_b0*uncert_b0]
   Sb_block4 = numpy.asmatrix(numpy.diag(variance_vals), dtype=float)

   Sb[0:N_bins, 0:N_bins] = Sb_block1
   Sb[N_bins:2*N_bins, N_bins:2*N_bins] = Sb_block2
   Sb[2*N_bins:2*N_bins+2, 2*N_bins:2*N_bins+2] = Sb_block3
   Sb[2*N_bins+2:2*N_bins+8, 2*N_bins+2:2*N_bins+8] = Sb_block4

   #Now build the complete Kb
   Kb = numpy.asmatrix(numpy.zeros((5, 2*N_bins+8), dtype=float))
   Kb[0,0:2*N_bins+2] = ddBZe_dB[:]
   #The snowfall parts are split up
   Kb[1,0:2*N_bins] = dP_db[0:2*N_bins]
   Kb[1,2*N_bins+2:2*N_bins+8] = dP_db[2*N_bins:2*N_bins+6]
   #Then finally the V_stats Jacobian terms fit in the last 6 columns
   Kb[2:5,2*N_bins+2:2*N_bins+8] = dV_stats_dB[:,:]

   #Now do the multiplication
   S_f_B_alt = Kb*Sb*Kb.T
   
   #Other forward model errors:  bias corrections and variances

   #Bias adjustment for precip rate
   #See notes 22 Oct 2009
   #est_precip_rate_adj = 0.51 - 0.22*b_coeff
   #Note - I don't think the precip rate bias adjustment is done right
   #It includes the effects of fitted fallspeeds, which shouldn't be part of this bias adjustment I think
   #For now set it to zero
   #Corrected using new evaluations of 2DVD data
   #bias_precip_rate_trunc = (-0.023*b_coeff + 0.083)*est_precip_rate
   bias_precip_rate_trunc = (-0.083*b_coeff - 0.177)*est_precip_rate   #corrected
   sigma_precip_rate_trunc = 0.14*est_precip_rate
   
   #Bias adjustment for radar reflectivity
   #bias_dBZe_trunc = -0.26*b_coeff - 0.38
   bias_dBZe_trunc = -0.61*b_coeff + 0.139        #corrected
   bias_dBZe_scat = -1.17 + 0.049*est_VertiX_dBZe
   #Now compute the forward model uncertainties
   #sigma_dBZe_scat is the uncertainty due to Rayleigh scattering errors
   #sigma_dBZe_trunc is the uncertainty due to truncation/discretization
   #sigma_dBZe_B is the uncertainty due to uncertainties in the parameters
   sigma_dBZe_scat = 0.65
   #sigma_dBZe_trunc   = 0.99*b_coeff - 0.78
   sigma_dBZe_trunc   = 1.845*b_coeff - 2.19      #corrected

   #Calculate the covariance between precip rate and dBZe errors due to the common
   #dependence on the truncated, discretized size distribution
   #corr_coeff = 0.35
   corr_coeff = 0.40                              #corrected
   covar_precip_rate_dBZe_trunc = corr_coeff*sigma_precip_rate_trunc*sigma_dBZe_trunc

   #Now apply bias corrections and set the Y_f matrix
   est_precip_rate = est_precip_rate - bias_precip_rate_trunc
   est_VertiX_dBZe = est_VertiX_dBZe - bias_dBZe_trunc - bias_dBZe_scat

   Y_f[0,0] = est_VertiX_dBZe
   Y_f[1,0] = est_precip_rate
   Y_f[2,0] = est_vtx_aves[0]
   Y_f[3,0] = est_vtx_aves[1]
   Y_f[4,0] = est_vtx_aves[2]


   #Now set the S_f matrix
   S_f = S_f_B_alt[:,:] + S_f_vtx[:,:]
   S_f[0,0] = S_f[0,0] + numpy.power(sigma_dBZe_scat,2) + numpy.power(sigma_dBZe_trunc,2)
   S_f[1,1] = S_f[1,1] + numpy.power(sigma_precip_rate_trunc,2)
   S_f[0,1] = S_f[0,1] + covar_precip_rate_dBZe_trunc
   S_f[1,0] = S_f[1,0] + covar_precip_rate_dBZe_trunc

   if dump_params_uncerts == True:
      #print sigma_dBZe_params
      print "P_Vstats_covar_vtx"
      print P_Vstats_covar_vtx
      print "S_f_V_stats_model"
      print S_f_V_stats_model

      print "S_f_vtx"
      print S_f_vtx
      #print f_coeff
      #for i_bin in range(N_bins):
      #   i_jac = N_bins + i_bin
      #   print "%4d %15.5e %15.5e %15.5e %15.5e   :  %15.5e %15.5e %15.5e %15.5e" %(i_jac, D_major_mm[i_bin], D_major_uncert_mm[i_bin], dP_db[i_bin], uncert_S_terms[i_bin], N_D_mm_mod[i_bin], N_D_uncert_mm_mod[i_bin], dP_db[i_jac], uncert_S_terms[i_jac])

   if diagnostics == True:
      masses_kg = particle_properties.mass_pow(D_major_mm*1.0e-3, \
                                               a_coeff,               \
                                               b_coeff)[0]
      (area_particles, area_circle) = particle_properties.area_pow(D_major_mm*1.0e-3, \
                                                                   gamma_coeff,       \
                                                                   sigma_coeff)
      mass_to_area_ratios = masses_kg*1000./area_particles
      area_ratios = area_particles/area_circle

      print "##In fmodel1:"
      print "## a, b, gamma, sigma:  %15.5e %15.5e %15.5e %15.5e" %(a_coeff, b_coeff, gamma_coeff, sigma_coeff)
      indices = numpy.where(N_D_mm > 0.)[0]
      for i_size in indices:
      #for i_size in range(len(D_major_mm)):
         print "#%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(D_major_mm[i_size], N_D_mm_mod[i_size], \
                masses_kg[i_size]*1000., area_particles[i_size], mass_to_area_ratios[i_size], vtx_ms[i_size], area_ratios[i_size])

   #I also want to compute the bias in the number-weighted fallspeed, in the final call to evaluate the forward model

   return(Y_f, S_f)

    
def fmodel1_jacobian( Xi,                \
                      N_D_mm,            \
                      N_D_uncert_mm,     \
                      D_major_mm,        \
                      D_major_uncert_mm, \
                      widths_mm,         \
                      T_K,               \
                      T_uncert_K,        \
                      P_hPa,             \
                      P_uncert_hPa,      \
                      freq,              \
                      K_sq_ice,          \
                      K_sq_ice_uncert,   \
                      K_sq_water,        \
                      K_sq_water_uncert):


   #*** Important Note ***
   #In this routine fmodel1_jacobian(), the first element of Xi, Xi[0,0] is referred to
   #as a_coeff.  It is actually (as of 25 Aug 2010) ln(a_coeff).  There's no need to change
   #the variable name.  What is desired is the jacobian with respect to ln(a_coeff), and that
   #is what is produced.
   #Ditto for gamma_coeff

   jacobian = numpy.asmatrix(numpy.zeros((5,5), dtype=float))
  
   #Perturb the parms and get the matrix of partial derivatives.  This is going to be slow
   a_coeff = Xi[0,0]
   b_coeff = Xi[1,0]
   gamma_coeff = Xi[2,0]
   sigma_coeff = Xi[3,0]
   f_coeff = Xi[4,0]

   a_coeff_pert = 0.01*a_coeff
   b_coeff_pert = 0.01*b_coeff
   gamma_coeff_pert = 0.01*gamma_coeff
   sigma_coeff_pert = 0.01*sigma_coeff
   f_coeff_pert = 0.01*f_coeff

   (Y_f_base, S_f_base) = fmodel1(Xi,     \
                       N_D_mm,             \
                       N_D_uncert_mm,      \
                       D_major_mm,         \
                       D_major_uncert_mm,  \
                       widths_mm,          \
                       T_K,                \
                       T_uncert_K,         \
                       P_hPa,              \
                       P_uncert_hPa,       \
                       freq,               \
                       K_sq_ice,           \
                       K_sq_ice_uncert,    \
                       K_sq_water,         \
                       K_sq_water_uncert,  \
                       diagnostics = False)

   Xi_pert = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
   Xi_pert[0,0] = a_coeff + a_coeff_pert
   Xi_pert[1,0] = b_coeff
   Xi_pert[2,0] = gamma_coeff
   Xi_pert[3,0] = sigma_coeff
   Xi_pert[4,0] = f_coeff
   (Y_f_pert, S_f_pert) = fmodel1(Xi_pert, \
                       N_D_mm,              \
                       N_D_uncert_mm,       \
                       D_major_mm,          \
                       D_major_uncert_mm,   \
                       widths_mm,           \
                       T_K,                 \
                       T_uncert_K,          \
                       P_hPa,               \
                       P_uncert_hPa,        \
                       freq,                \
                       K_sq_ice,            \
                       K_sq_ice_uncert,     \
                       K_sq_water,          \
                       K_sq_water_uncert)

   jacobian[0,0] = (Y_f_pert[0,0] - Y_f_base[0,0])/a_coeff_pert
   jacobian[1,0] = (Y_f_pert[1,0] - Y_f_base[1,0])/a_coeff_pert
   jacobian[2,0] = (Y_f_pert[2,0] - Y_f_base[2,0])/a_coeff_pert
   jacobian[3,0] = (Y_f_pert[3,0] - Y_f_base[3,0])/a_coeff_pert
   jacobian[4,0] = (Y_f_pert[4,0] - Y_f_base[4,0])/a_coeff_pert
  
   Xi_pert[0,0] = a_coeff
   Xi_pert[1,0] = b_coeff + b_coeff_pert
   Xi_pert[2,0] = gamma_coeff
   Xi_pert[3,0] = sigma_coeff
   Xi_pert[4,0] = f_coeff
   (Y_f_pert, S_f_pert) = fmodel1(Xi_pert, \
                       N_D_mm,              \
                       N_D_uncert_mm,       \
                       D_major_mm,          \
                       D_major_uncert_mm,   \
                       widths_mm,           \
                       T_K,                 \
                       T_uncert_K,          \
                       P_hPa,               \
                       P_uncert_hPa,        \
                       freq,                \
                       K_sq_ice,            \
                       K_sq_ice_uncert,     \
                       K_sq_water,          \
                       K_sq_water_uncert)

   jacobian[0,1] = (Y_f_pert[0,0] - Y_f_base[0,0])/b_coeff_pert
   jacobian[1,1] = (Y_f_pert[1,0] - Y_f_base[1,0])/b_coeff_pert
   jacobian[2,1] = (Y_f_pert[2,0] - Y_f_base[2,0])/b_coeff_pert
   jacobian[3,1] = (Y_f_pert[3,0] - Y_f_base[3,0])/b_coeff_pert
   jacobian[4,1] = (Y_f_pert[4,0] - Y_f_base[4,0])/b_coeff_pert
  

   Xi_pert[0,0] = a_coeff
   Xi_pert[1,0] = b_coeff
   Xi_pert[2,0] = gamma_coeff + gamma_coeff_pert
   Xi_pert[3,0] = sigma_coeff
   Xi_pert[4,0] = f_coeff
   (Y_f_pert, S_f_pert) = fmodel1(Xi_pert, \
                       N_D_mm,              \
                       N_D_uncert_mm,       \
                       D_major_mm,          \
                       D_major_uncert_mm,   \
                       widths_mm,           \
                       T_K,                 \
                       T_uncert_K,          \
                       P_hPa,               \
                       P_uncert_hPa,        \
                       freq,                \
                       K_sq_ice,            \
                       K_sq_ice_uncert,     \
                       K_sq_water,          \
                       K_sq_water_uncert)

   jacobian[0,2] = (Y_f_pert[0,0] - Y_f_base[0,0])/gamma_coeff_pert
   jacobian[1,2] = (Y_f_pert[1,0] - Y_f_base[1,0])/gamma_coeff_pert
   jacobian[2,2] = (Y_f_pert[2,0] - Y_f_base[2,0])/gamma_coeff_pert
   jacobian[3,2] = (Y_f_pert[3,0] - Y_f_base[3,0])/gamma_coeff_pert
   jacobian[4,2] = (Y_f_pert[4,0] - Y_f_base[4,0])/gamma_coeff_pert
  

   Xi_pert[0,0] = a_coeff
   Xi_pert[1,0] = b_coeff
   Xi_pert[2,0] = gamma_coeff
   Xi_pert[3,0] = sigma_coeff + sigma_coeff_pert
   Xi_pert[4,0] = f_coeff
   (Y_f_pert, S_f_pert) = fmodel1(Xi_pert, \
                       N_D_mm,              \
                       N_D_uncert_mm,       \
                       D_major_mm,          \
                       D_major_uncert_mm,   \
                       widths_mm,           \
                       T_K,                 \
                       T_uncert_K,          \
                       P_hPa,               \
                       P_uncert_hPa,        \
                       freq,                \
                       K_sq_ice,            \
                       K_sq_ice_uncert,     \
                       K_sq_water,          \
                       K_sq_water_uncert)

   jacobian[0,3] = (Y_f_pert[0,0] - Y_f_base[0,0])/sigma_coeff_pert
   jacobian[1,3] = (Y_f_pert[1,0] - Y_f_base[1,0])/sigma_coeff_pert
   jacobian[2,3] = (Y_f_pert[2,0] - Y_f_base[2,0])/sigma_coeff_pert
   jacobian[3,3] = (Y_f_pert[3,0] - Y_f_base[3,0])/sigma_coeff_pert
   jacobian[4,3] = (Y_f_pert[4,0] - Y_f_base[4,0])/sigma_coeff_pert
  
   Xi_pert[0,0] = a_coeff
   Xi_pert[1,0] = b_coeff
   Xi_pert[2,0] = gamma_coeff
   Xi_pert[3,0] = sigma_coeff
   Xi_pert[4,0] = f_coeff + f_coeff_pert
   (Y_f_pert, S_f_pert) = fmodel1(Xi_pert, \
                       N_D_mm,              \
                       N_D_uncert_mm,       \
                       D_major_mm,          \
                       D_major_uncert_mm,   \
                       widths_mm,           \
                       T_K,                 \
                       T_uncert_K,          \
                       P_hPa,               \
                       P_uncert_hPa,        \
                       freq,                \
                       K_sq_ice,            \
                       K_sq_ice_uncert,     \
                       K_sq_water,          \
                       K_sq_water_uncert)

   jacobian[0,4] = (Y_f_pert[0,0] - Y_f_base[0,0])/f_coeff_pert
   jacobian[1,4] = (Y_f_pert[1,0] - Y_f_base[1,0])/f_coeff_pert
   jacobian[2,4] = (Y_f_pert[2,0] - Y_f_base[2,0])/f_coeff_pert
   jacobian[3,4] = (Y_f_pert[3,0] - Y_f_base[3,0])/f_coeff_pert
   jacobian[4,4] = (Y_f_pert[4,0] - Y_f_base[4,0])/f_coeff_pert
   return jacobian

if __name__ == "__main__":
   def mytest():
      (delta0, C0, a0, b0) = fallspeed_forward_model.get_parameters()
      D_major_mm = numpy.zeros((131,), dtype=float)
      D_major_mm[0] = 0.25
      for i_size in range(1, len(D_major_mm)):
        D_major_mm[i_size] = D_major_mm[i_size-1]+0.25
      widths_mm = numpy.ones((131,), dtype=float)*0.25

      N0_est = 2.0e3
      lambda_est = 1.0
      N_D_mm = N0_est*numpy.exp(-lambda_est*D_major_mm)
    
      a_coeff = 0.005
      b_coeff = 2.0
      gamma_coeff = 0.3
      sigma_coeff = 1.88
      f_coeff = 1.0

      myparams = numpy.asmatrix(numpy.zeros((5,1), dtype=float))
      myparams[0,0] = a_coeff
      myparams[1,0] = b_coeff
      myparams[2,0] = gamma_coeff
      myparams[3,0] = sigma_coeff
      myparams[4,0] = f_coeff

      f_radar = 9.35
      K_sq_ice_radar = 0.177
      K_sq_water_radar = 0.93

      T_K = 253.
      P_hPa = 1000.

      obs_vtx_ms = fallspeed_forward_model.fallspeed(D_major_mm, \
                                                     a_coeff,        \
                                                     b_coeff,        \
                                                     gamma_coeff,    \
                                                     sigma_coeff,    \
                                                     T_K,            \
                                                     P_hPa,          \
                                                     delta0,         \
                                                     C0,             \
                                                     a0,             \
                                                     b0)
      print "obs_vtx_ms:"
      print obs_vtx_ms

      obs_vtx_sigmas_ms = obs_vtx_ms*0.5
      (Y_f,S_f) = fmodel1(myparams,          \
                     N_D_mm,            \
                     D_major_mm,        \
                     widths_mm,         \
                     T_K,               \
                     P_hPa,             \
                     f_radar,           \
                     K_sq_ice_radar,    \
                     K_sq_water_radar,  \
                     lambda_est)

      print "X at start:" , myparams[0,0], myparams[1,0], myparams[2,0], myparams[3,0], myparams[4,0]
      print "Y_f at start (synthetic observations):", Y_f[0,0], Y_f[1,0],  Y_f[2,0], Y_f[3,0], Y_f[4,0]
      dY_f_dXi = fmodel1_jacobian( myparams,          \
                                   N_D_mm,            \
                                   D_major_mm,        \
                                   widths_mm,         \
                                   obs_vtx_ms,        \
                                   obs_vtx_sigmas_ms, \
                                   T_K,               \
                                   P_hPa,             \
                                   f_radar,           \
                                   K_sq_ice_radar,    \
                                   K_sq_water_radar,  \
                                   lambda_est)

      #Try a retrieval
      #Observations
      Y = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
      Y[0,0] = Y_f[0,0]
      Y[1,0] = Y_f[1,0]
      Y[2,0] = Y_f[2,0]
      Y[3,0] = Y_f[3,0]
      Y[4,0] = Y_f[4,0]

      #Observation and forward model uncertainty
      Se = numpy.asmatrix(numpy.zeros((5,5),dtype=float))
      Se[0,0] = 3.0*3.0
      Se[1,1] = 0.2*0.2
      Se[2,2] = 0.05*0.05
      Se[3,3] = 0.05*0.05
      Se[4,4] = 0.05*0.05

      #Set up retrieval a priori      
      Xa = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
      Xa[0] = 0.01
      Xa[1] = 2.2
      Xa[2] = 0.3
      Xa[3] = 2.0
      Xa[4] = 1.0
      Sa = numpy.asmatrix(numpy.zeros((5,5),dtype=float))
      Sa[0,0] = 0.1*0.1
      Sa[1,1] = 1.1*1.1
      Sa[2,2] = 0.1*0.1
      Sa[3,3] = 3.0*3.0
      Sa[4,4] = 0.5*0.5
       
      (Y_ap,S_ap) = fmodel1(Xa,          \
                     N_D_mm,            \
                     D_major_mm,        \
                     widths_mm,         \
                     T_K,               \
                     P_hPa,             \
                     f_radar,           \
                     K_sq_ice_radar,    \
                     K_sq_water_radar,  \
                     lambda_est)

      print "Xa:" , myparams[0,0], myparams[1,0], myparams[2,0], myparams[3,0]
      print "Y_ap:", Y_f[0,0], Y_f[1,0],  Y_f[2,0]


      results = massfit1_oe(Y,                  \
                            Se,                 \
                            Xa,                 \
                            Sa,                 \
                            N_D_mm,             \
                            D_major_mm,         \
                            widths_mm,          \
                            obs_vtx_ms,         \
                            obs_vtx_sigmas_ms,  \
                            T_K,                \
                            P_hPa,              \
                            f_radar,            \
                            K_sq_ice_radar,     \
                            K_sq_water_radar,   \
                            lambda_est,          \
                            diagnostics = True)

      X_fitted = results[0]
      chi_sq = results[1]
      status = results[2]
      A_matrix = results[3]
      print "X_fitted:  "
      print X_fitted
      print "A_matrix:   "
      print A_matrix
      #print A_matrix.trace()
      
      #Evaluate the forward model
      (Y_fitted, S_fitted) = fmodel1(X_fitted,           \
                          N_D_mm,             \
                          D_major_mm,         \
                          widths_mm,          \
                          T_K,                \
                          P_hPa,              \
                          f_radar,            \
                          K_sq_ice_radar,     \
                          K_sq_water_radar,   \
                          lambda_est)

      print "Y_fitted:   "
      print Y_fitted


      est_vtx_ms_subset = fallspeed_forward_model.fallspeed(D_major_mm, \
                                                            X_fitted[0,0],  \
                                                            X_fitted[1,0],  \
                                                            X_fitted[2,0],  \
                                                            X_fitted[3,0],  \
                                                            T_K,            \
                                                            P_hPa,          \
                                                            delta0,         \
                                                            C0,             \
                                                            a0,             \
                                                            b0) 

      for i_size in range(len(D_major_mm)):
          print "%15.5e %15.5e %15.5e" %(D_major_mm[i_size], est_vtx_ms_subset[i_size], obs_vtx_ms[i_size])


   def mytest_fmodel():
      print fallspeed_forward_model.initialized
      Xi = numpy.asarray(numpy.zeros((5,1), dtype=float))
      #Xi[0,0] = 0.00348925
      Xi[0,0] = -5.658
      Xi[1,0] = 2.09026
      #Xi[2,0] = 0.284172
      Xi[2,0] = -1.2582
      Xi[3,0] = 1.98306
      Xi[4,0] = 0.85

      #Regime E
      N0_obs_mm = 10.**4.43
      lambda_obs_mm = 1.017
      #a priori
      Xi[0,0] = -6.181
      Xi[1,0] = 2.067
      Xi[2,0] = -1.556
      Xi[3,0] = 1.785
      Xi[4,0] = 0.825

      #retrieved
      Xi[0,0] = -6.396
      Xi[1,0] = 2.14
      Xi[2,0] = -1.673
      Xi[3,0] = 1.976
      Xi[4,0] = 0.853
     



      T_K = 255.
      P_hPa = 1000.
      freq = 9.35
      K_sq_ice = 0.177
      K_sq_water = 0.93

      D_obs_mm = numpy.linspace(0., 26., 105)
      D_obs_mm_uncert = numpy.zeros((105,), dtype=float)
      widths_obs_mm = numpy.ones((105,), dtype=float)*0.25


      N_D_obs_mm = N0_obs_mm*numpy.exp(-lambda_obs_mm*D_obs_mm)
      N_D_obs_mm_uncert = numpy.copy(N_D_obs_mm)
      N_D_obs_mm_uncert[:] = 0.
 
      (Y_f, S_f) = fmodel1(Xi, N_D_obs_mm, N_D_obs_mm_uncert, D_obs_mm, D_obs_mm_uncert, widths_obs_mm, T_K, 0., P_hPa, 0., freq, K_sq_ice, 0., K_sq_water, 0.)
      print Y_f

      Xi[4,0] = 0.85+0.0085

      (Y_f_new, S_f_new) = fmodel1(Xi, N_D_obs_mm, N_D_obs_mm_uncert, D_obs_mm, D_obs_mm_uncert, widths_obs_mm, T_K, 0., P_hPa, 0., freq, K_sq_ice, 0., K_sq_water, 0.)
      print Y_f[0,0] , Y_f_new[0,0], (Y_f_new[0,0] - Y_f[0,0])/0.0085

   #mytest()
   mytest_fmodel()
