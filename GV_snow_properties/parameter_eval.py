#!/usr/bin/env python


"""Driver routine for GV_snow_properties
"""

#--------------------------------------------------------------------------
# Driver routine for GV_snow_properties
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------



import string, sys, numpy, scipy.optimize, numpy.linalg, scipy.integrate

import ret_config
import inputs
import ap
import precip_img_pkg, twodvd, radar, precip, met

import massfit
import forward_models_oe

#import ic_diagnostics

import GV_tools.gcpex.dataproc.pip

def near(x, y):
    atol = 1.e-08
    rtol = 1.e-05
    mask = numpy.less_equal(numpy.abs(x - y), atol + rtol*numpy.abs(y))
    return mask

#numpy.seterr(divide='raise', invalid='raise')


data_dir = ret_config.config.get('Main', 'data_dir')
events_string = ret_config.config.get('Case', 'event')
locations_string = ret_config.config.get('Case', 'location')
mrr_instruments_string = ret_config.config.get('Case', 'MRR_instrument')
pip_instruments_string = ret_config.config.get('Case', 'PIP_instrument')

events = [s for s in events_string.split(',') if len(s.strip()) != 0]
locations = [s for s in locations_string.split(',') if len(s.strip()) != 0]
mrr_instruments = [s for s in mrr_instruments_string.split(',') if len(s.strip()) != 0]
pip_instruments = [s for s in pip_instruments_string.split(',') if len(s.strip()) != 0]


for event in events:
    for location in locations:
        for mrr_instrument in mrr_instruments:
            for pip_instrument in pip_instruments:

                print event, location, mrr_instrument, pip_instrument

                (mrr_fpath,
                 pip_fpath,
                 twodvd_fpath,
                 precip_fpath,
                 met_fpath,
                 N_times) = inputs.prep_gcpex(event, location, mrr_instrument, pip_instrument, data_dir)
                
                fname_mass_out = "%s_%s_%s_%s_mass.txt" %(event, location, mrr_instrument, pip_instrument)
                fname_area_out = "%s_%s_%s_%s_area.txt" %(event, location, mrr_instrument, pip_instrument)
                fname_phi_out = "%s_%s_%s_%s_phi.txt" %(event, location, mrr_instrument, pip_instrument)

                f_ptr_mass = open(fname_mass_out, 'w')
                f_ptr_area = open(fname_area_out, 'w')
                f_ptr_phi = open(fname_phi_out, 'w')
               
                f_ptr_mass.write('#%s %s %s %s\n' %(location, event, mrr_instrument, pip_instrument)) 
                f_ptr_mass.write('#%s\n' %(mrr_fpath,))
                f_ptr_mass.write('#%s\n' %(pip_fpath,))
                f_ptr_mass.write('#%s\n' %(twodvd_fpath,))
                f_ptr_mass.write('#%s\n' %(precip_fpath,))
                f_ptr_mass.write('#%s\n' %(met_fpath,))

                f_ptr_area.write('#%s %s %s %s\n' %(location, event, mrr_instrument, pip_instrument)) 
                f_ptr_area.write('#%s\n' %(mrr_fpath,))
                f_ptr_area.write('#%s\n' %(pip_fpath,))
                f_ptr_area.write('#%s\n' %(twodvd_fpath,))
                f_ptr_area.write('#%s\n' %(precip_fpath,))
                f_ptr_area.write('#%s\n' %(met_fpath,))

                f_ptr_phi.write('#%s %s %s %s\n' %(location, event, mrr_instrument, pip_instrument)) 
                f_ptr_phi.write('#%s\n' %(mrr_fpath,))
                f_ptr_phi.write('#%s\n' %(pip_fpath,))
                f_ptr_phi.write('#%s\n' %(twodvd_fpath,))
                f_ptr_phi.write('#%s\n' %(precip_fpath,))
                f_ptr_phi.write('#%s\n' %(met_fpath,))


                PIP_obs = precip_img_pkg.init(pip_fpath)
                TwoDVD_obs = twodvd.init(twodvd_fpath)
                precip_obs = precip.init(precip_fpath)
                met_obs = met.init(met_fpath)
                MRR_obs = radar.init(mrr_fpath)

                ##Radar stuff
                ##MRR settings
                em_data = radar.em_dict['MRR']
                radar_obs_freq = em_data.freq
                radar_obs_K_sq_ice = em_data.K_sq_ice
                radar_obs_K_sq_water = em_data.K_sq_water
                radar_obs_K_sq_ice_uncert = em_data.K_sq_ice_uncert
                radar_obs_K_sq_water_uncert = em_data.K_sq_water_uncert
                
                #Set the a priori, observation and model-measurement uncertainties
                Xa = ap.Xa
                Sa = ap.Sa
                Se = numpy.asmatrix(numpy.zeros((5,5),dtype=float))
                Y = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
                
                #Some other universal uncertainties
                T_uncert = met_obs.s_T
                P_uncert = met_obs.s_P
                
                PIP_sample_vols_m3 = precip_img_pkg.sample_volumes(PIP_obs.Di, PIP_obs.dt_sampling)
                
                for i_time in range(N_times):
                
                    Ntot = numpy.sum(PIP_obs.N_Di[i_time, :]*PIP_obs.dDi*PIP_sample_vols_m3)
                    Nconc = scipy.integrate.trapz(PIP_obs.N_Di[i_time, :], PIP_obs.Di, axis=0)
                    bin_count = numpy.sum(numpy.greater(PIP_obs.N_Di[i_time, :], 0.))    
                
                    if Ntot > 350. and bin_count > 3:
                        N_D_sigma = PIP_obs.s_N_Di
                        D_sigma = PIP_obs.s_Di
                
                        if bin_count > 3:
                            #Get the matching data
                            #First the mean precip rate
                
                            precip_rate = precip_obs.rate[i_time]
                            #print "Tests:", Ntot, Nconc, bin_count, precip_rate
                
                            if precip_rate > 0.:
                                Ze = MRR_obs.Ze[i_time]
                                T = met_obs.T[i_time]
                                P = met_obs.P[i_time]
                                u = met_obs.u_2m[i_time]
                
                                #Fit the distribution to get estimates of lambda, N0
                                N0_est, lambda_est = GV_tools.gcpex.dataproc.pip.best_fit_ND_exp(PIP_obs.Di,
                                                                                                 PIP_obs.N_Di[i_time,:],
                                                                                                 PIP_obs.s_N_Di[i_time,:])
                                #print N0_est, lambda_est
                
                                #Get the 2DVD data
                                V_stats = TwoDVD_obs.V_stats[i_time]
                                S_eps_V_stats = TwoDVD_obs.S_eps_V_stats[i_time]
                
                                #print "Y_obs:", Ze, precip_rate, V_stats[0], V_stats[1], V_stats[2]
                
                                if not numpy.any(near(V_stats, -999.)):
                
                                    Y[0,0] = Ze
                                    Y[1,0] = precip_rate
                                    
                                    Y[2,0] = V_stats[0]
                                    Y[3,0] = V_stats[1]
                                    Y[4,0] = V_stats[2]
                
                                    #Se[0,0] = dBZe_VertiX_uncert*dBZe_VertiX_uncert
                                    Se[0,0] = 2.
                                    Se[1,1] = precip_obs.s_rate[i_time]*precip_obs.s_rate[i_time]
                
                                    
                                    Se[2:5,2:5] = S_eps_V_stats
                
                                    #Now do the OE fit
                                    #All the rescaling (D_obs to D_max and N(D_obs) to N(D_max)) is done within the forward model
                                    results = massfit.massfit1_oe(Y,                       \
                                                                  Se,                      \
                                                                  Xa,                      \
                                                                  Sa,                      \
                                                                  PIP_obs.N_Di[i_time,:],  \
                                                                  PIP_obs.s_N_Di[i_time,:],\
                                                                  PIP_obs.Di,              \
                                                                  PIP_obs.s_Di[i_time,:],  \
                                                                  PIP_obs.dDi,             \
                                                                  T,                       \
                                                                  T_uncert,                \
                                                                  P,                       \
                                                                  P_uncert,                \
                                                                  radar_obs_freq,          \
                                                                  radar_obs_K_sq_ice,      \
                                                                  radar_obs_K_sq_ice_uncert, \
                                                                  radar_obs_K_sq_water,    \
                                                                  radar_obs_K_sq_water_uncert, \
                                                                  diagnostics = True)
                
                                    #print "Results"
                                    #print results[0], results[1], results[2]
                                    ln_alpha, beta, ln_gamma, sigma, phi = results[0]
                
                
                           
                                    status = results[2]
                                    if status == 0:
                                        #Successful retrieval
                                        X_fitted = results[0]
                                        chi_sq = results[1]
                                        status = results[2]
                                        A_matrix = results[3]
                                        cov_matrix = results[4]
                
                                        (Y_f, S_f) = forward_models_oe.fmodel1(X_fitted,      \
                                                                               PIP_obs.N_Di[i_time, :],   \
                                                                               PIP_obs.s_N_Di[i_time, :], \
                                                                               PIP_obs.Di,
                                                                               PIP_obs.s_Di[i_time, :], \
                                                                               PIP_obs.dDi,  \
                                                                               T,            \
                                                                               T_uncert,     \
                                                                               P,          \
                                                                               P_uncert,   \
                                                                               radar_obs_freq,     \
                                                                               radar_obs_K_sq_ice, \
                                                                               radar_obs_K_sq_ice_uncert, \
                                                                               radar_obs_K_sq_water, \
                                                                               radar_obs_K_sq_water_uncert, \
                                                                               diagnostics = False,         \
                                                                               dump_params_uncerts = False)

                                        sd_X_fitted = numpy.asmatrix(numpy.zeros((5,1), dtype=float))
                                        try:
                                            sd_X_fitted[0,0] = numpy.sqrt(cov_matrix[0,0])
                                            sd_X_fitted[1,0] = numpy.sqrt(cov_matrix[1,1])
                                            sd_X_fitted[2,0] = numpy.sqrt(cov_matrix[2,2])
                                            sd_X_fitted[3,0] = numpy.sqrt(cov_matrix[3,3])
                                            sd_X_fitted[4,0] = numpy.sqrt(cov_matrix[4,4])
                                        except:
                                            print "Negative variance"
                                            numpy.set_printoptions(linewidth=132)
                                            print S_f
                                            print cov_matrix
                                            numpy.set_printoptions(linewidth=75)
                    
                                            assert 0
                                        if numpy.any(numpy.isnan(sd_X_fitted)):
                                            print "Negative variance"
                                            numpy.set_printoptions(linewidth=132)
                                            print S_f
                                            print " "
                                            print cov_matrix
                                            numpy.set_printoptions(linewidth=75)
                 
                                        f_ptr_mass.write("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e\n" %(beta, sd_X_fitted[1,0], ln_alpha, sd_X_fitted[0,0], Ze, 0., T, N0_est, lambda_est))
                                        f_ptr_area.write("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e\n" %(sigma, sd_X_fitted[3,0], ln_gamma, sd_X_fitted[2,0], Ze, 0., T, N0_est, lambda_est))
                                        f_ptr_phi.write("%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e\n" %(phi, sd_X_fitted[4,0], Ze, 0., T, N0_est, lambda_est))
                f_ptr_mass.close()
                f_ptr_area.close()
                f_ptr_phi.close()
                
                # 
                #
                #                    
                #
                #                     #Compute the correlation matrix
                #                     corr_matrix = numpy.copy(cov_matrix)
                #                     for i_r in range(4):
                #                        for i_c in range(4):
                #                           corr_matrix[i_r,i_c] = cov_matrix[i_r,i_c]/numpy.sqrt(cov_matrix[i_r,i_r]*cov_matrix[i_c,i_c])
                #                    
                #                     if chi_sq < 10.:
                #                        normed_chi_sq = chi_sq/5.
                #
                #                        #Get the IC diagnostics, need the Jacobian first
                #                        K_f = forward_models_oe.fmodel1_jacobian(X_fitted,      \
                #                                           N_D_mm_obs_aug,     \
                #                                           N_D_mm_obs_uncert_aug, \
                #                                           D_major_mm_obs_aug, \
                #                                           D_major_mm_obs_uncert_aug, \
                #                                           widths_mm_obs_aug,  \
                #                                           T_K_obs,            \
                #                                           T_uncert_K_obs,     \
                #                                           P_hPa_obs,          \
                #                                           P_uncert_hPa_obs,   \
                #                                           radar_obs_freq,     \
                #                                           radar_obs_K_sq_ice, \
                #                                           radar_obs_K_sq_ice_uncert, \
                #                                           radar_obs_K_sq_water, \
                #                                           radar_obs_K_sq_water_uncert)
                #
                #                        #Compute information content diagnostics
                #                        SIC, dof_signal, eff_rank = ic_diagnostics.diagnostics(Sa, cov_matrix, Se+S_f, K_f)
                #
                #                         
                #                      
                #                        ##Retrieved values and variances
                #                        #print "#%15.8e %4d %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %8.3f %4d" % \
                #                        #       (target_time_mid, status, Y[0,0], Y_f[0,0], Y[1,0], Y_f[1,0], Y[2,0], Y_f[2,0], Y[3,0], Y_f[3,0], Y[4,0], Y_f[4,0], X_fitted[0,0],\
                #                        #        sd_X_fitted[0,0], X_fitted[1,0], sd_X_fitted[1,0], X_fitted[2,0], sd_X_fitted[2,0], X_fitted[3,0], sd_X_fitted[3,0], X_fitted[4,0], sd_X_fitted[4,0], \
                #                        #        normed_chi_sq, A_matrix.trace(), T_K_obs, N0_mm_est, lambda_mm_est, X_fitted[4,0]*lambda_mm_est, Ntot, Ze_VertiX_frac_sd, VertiX_count_nonP) 
                #
                #                        #Retrieved values and variances
                #                        print "#%15.8e %4d %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %8.3f %4d %15.5e %15.5e %15.5e" % \
                #                               (target_time_mid, status, Y[0,0], Y_f[0,0], Y[1,0], Y_f[1,0], Y[2,0], Y_f[2,0], Y[3,0], Y_f[3,0], Y[4,0], Y_f[4,0], X_fitted[0,0],\
                #                                sd_X_fitted[0,0], X_fitted[1,0], sd_X_fitted[1,0], X_fitted[2,0], sd_X_fitted[2,0], X_fitted[3,0], sd_X_fitted[3,0], X_fitted[4,0], sd_X_fitted[4,0], \
                #                                normed_chi_sq, A_matrix.trace(), T_K_obs, N0_mm_est, lambda_mm_est, X_fitted[4,0]*lambda_mm_est, Ntot, Ze_VertiX_frac_sd, VertiX_count_nonP, SIC, dof_signal, eff_rank) 
                #
                #
                #                        #Augmented covariance output
                #                        print "###Covariance: %15.8e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" \
                #                           %(target_time_mid, X_fitted[0,0], X_fitted[1,0], X_fitted[2,0], X_fitted[3,0], X_fitted[4,0], \
                #                           cov_matrix[0,0], cov_matrix[0,1], cov_matrix[0,2], cov_matrix[0,3], cov_matrix[0,4], cov_matrix[1,0], cov_matrix[1,1], cov_matrix[1,2], cov_matrix[1,3], cov_matrix[1,4], \
                #                           cov_matrix[2,0], cov_matrix[2,1], cov_matrix[2,2], cov_matrix[2,3], cov_matrix[2,4], cov_matrix[3,0], cov_matrix[3,1], cov_matrix[3,2], cov_matrix[3,3], cov_matrix[3,4], \
                #                           cov_matrix[4,0], cov_matrix[4,1], cov_matrix[4,2], cov_matrix[4,3], cov_matrix[4,4])
                #                
                #
                #                        #A-matrix output
                #                        print "##%15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e %15.5e" %(N0_mm_est, lambda_mm_est, Y[0,0], Y[1,0], A_matrix[0,0], A_matrix[1,1], A_matrix[2,2], A_matrix[3,3], A_matrix[4,4])
                #  
                #                  else:
                #                     #Unsuccessful retrieval
                #                     pass
