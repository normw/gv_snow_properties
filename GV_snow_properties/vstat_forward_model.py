#!/usr/bin/env python


"""Forward model for 3-bin velocity properties
"""

#--------------------------------------------------------------------------
# Forward model for 3-bin velocity properties
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------



import numpy
import fallspeed_forward_model

#These are the sizes at which the fallpseed stats are calculated
D_vtx_stats_mm = numpy.array((4.0, 2.0, 1.0), dtype=float)
widths_vtx_stats_mm = numpy.array((0.5, 0.25, 0.25), dtype=float)

fallspeed_forward_model.init('correlation_matrix_MH2005_5x5.txt', 'MH2005')
#fallspeed_forward_model.init('correlation_matrix_HW2010_5x5.txt', 'HW2010')

def V_stats(a_coeff, b_coeff, gamma_coeff, sigma_coeff, T_K, P_hPa, D_major_mm):
   global D_vtx_stats_mm, widths_vtx_stats_mm
  
   (delta0, C0, a0, b0) = fallspeed_forward_model.get_parameters() 
   #Just get the fallspeeds at each of the 3 values of D_vstats_mm
   #but need a, b, gamma, sigma, T, P
   #Get the fallspeeds along with the covariance matrix at the D_vstats_mm values
   #assuming fractional uncertainty of 30%
   fractional_vtx_uncert = 0.30
   #Highacc - high accuracy case
   #fractional_vtx_uncert = 0.10

   (est_vtx_ms, vtx_covar_matrix) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,  \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0,               \
                                                                       fractional_vtx_uncert)

   vt0 = est_vtx_ms[0]
   vt1 = est_vtx_ms[1]
   vt2 = est_vtx_ms[2]
   Delta1 = vt0 - vt1
   Delta2 = vt0 - vt2


   #Make vectors for the derivatives dV0/dv, d(V1)/dv and d(V2)/dv
   #Make the matrix to hold d(Vstats)/dvtx
   N_bins = len(D_major_mm)
   dVstats_dvtx = numpy.asmatrix(numpy.zeros((3,N_bins,), dtype=float))
   dV0_dvtx = numpy.zeros((N_bins,), dtype=float)
   dV1_dvtx = numpy.zeros((N_bins,), dtype=float)
   dV2_dvtx = numpy.zeros((N_bins,), dtype=float)
   #Find the indices of D_major_mm nearest to D_vtx_stats_mm
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[0])
   i_D0 = numpy.argmin(deltas)
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[1])
   i_D1 = numpy.argmin(deltas)
   deltas = numpy.abs(D_major_mm - D_vtx_stats_mm[2])
   i_D2 = numpy.argmin(deltas)
   dV0_dvtx[i_D0] = 1.
   dV1_dvtx[i_D1] = 1.
   dV2_dvtx[i_D2] = 1.

   d_DeltaV1_dvtx = dV0_dvtx - dV1_dvtx
   d_DeltaV2_dvtx = dV0_dvtx - dV2_dvtx

   dVstats_dvtx[0,:] = dV0_dvtx[:]
   dVstats_dvtx[1,:] = d_DeltaV1_dvtx[:]
   dVstats_dvtx[2,:] = d_DeltaV2_dvtx[:]


   #Do the Jacobian for vt0, Delta1 and Delta2
   #Row 1 is for d_vt0/dB, Row 2 is for d_Delta1/dB, and Row 3 is for d_Delta2/dB
   #and there are 9 "B" variables (D0, D1, D2, T, P, delta0, C0, a0, b0)
   #Revised because D0, D1 and D2 aren't uncertain, they're specified
   #So dV_stats_dB is now 3x6
   #dV_stats_dB = numpy.asmatrix(numpy.zeros((3, 9), dtype=float))
   dV_stats_dB = numpy.asmatrix(numpy.zeros((3, 6), dtype=float))
   vt0_base = vt0
   vt1_base = vt1
   vt2_base = vt2
   Delta1_base = Delta1
   Delta2_base = Delta2


   #Get the perturbations based on other perturbed parameters
   #First T_K
   delta_T_K = T_K*0.01
   T_K_pert = T_K + delta_T_K
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K_pert,         \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0)
   dV_stats_dB[0,0] = (vt0_pert - vt0_base)/delta_T_K
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,0] = (Delta1_pert - Delta1_base)/delta_T_K
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,0] = (Delta2_pert - Delta2_base)/delta_T_K


   #Next P_hPa
   delta_P_hPa = P_hPa*0.01
   P_hPa_pert = P_hPa + delta_P_hPa
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa_pert,       \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0)
   
   dV_stats_dB[0,1] = (vt0_pert - vt0_base)/delta_P_hPa
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,1] = (Delta1_pert - Delta1_base)/delta_P_hPa
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,1] = (Delta2_pert - Delta2_base)/delta_P_hPa

   #Next delta0
   delta_delta0 = delta0*0.01
   delta0_pert = delta0 + delta_delta0
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0_pert,      \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0)
   
   dV_stats_dB[0,2] = (vt0_pert - vt0_base)/delta_delta0
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,2] = (Delta1_pert - Delta1_base)/delta_delta0
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,2] = (Delta2_pert - Delta2_base)/delta_delta0

   #Next C0
   delta_C0 = C0*0.01
   C0_pert = C0 + delta_C0
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0_pert,          \
                                                                       a0,               \
                                                                       b0)
   
   dV_stats_dB[0,3] = (vt0_pert - vt0_base)/delta_C0
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,3] = (Delta1_pert - Delta1_base)/delta_C0
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,3] = (Delta2_pert - Delta2_base)/delta_C0

   #Next a0
   #a0 may be zero sometimes
   delta_a0 = numpy.max([a0*0.01, 0.0017*0.01])
   a0_pert = a0 + delta_a0
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0_pert,          \
                                                                       b0)
   
   dV_stats_dB[0,4] = (vt0_pert - vt0_base)/delta_a0
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,4] = (Delta1_pert - Delta1_base)/delta_a0
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,4] = (Delta2_pert - Delta2_base)/delta_a0

   #Finally b0
   #b0 may be zero sometimes
   delta_b0 = numpy.max([b0*0.01, 0.8*0.01])
   b0_pert = b0 + delta_b0
   (vt0_pert, vt1_pert, vt2_pert) =  fallspeed_forward_model.fallspeed(D_vtx_stats_mm,   \
                                                                       a_coeff,          \
                                                                       b_coeff,          \
                                                                       gamma_coeff,      \
                                                                       sigma_coeff,      \
                                                                       T_K,              \
                                                                       P_hPa,            \
                                                                       delta0,           \
                                                                       C0,               \
                                                                       a0,               \
                                                                       b0_pert)
   
   dV_stats_dB[0,5] = (vt0_pert - vt0_base)/delta_b0
   Delta1_pert = vt0_pert - vt1_pert
   dV_stats_dB[1,5] = (Delta1_pert - Delta1_base)/delta_b0
   Delta2_pert = vt0_pert - vt2_pert
   dV_stats_dB[2,5] = (Delta2_pert - Delta2_base)/delta_b0

   #Done with the Jacobian dV_stats_dB

   #Now need the forward model uncertainties
   #Use the covariance matrix computed when the base fallspeeds were evaluated above
   #Need a 3x3 matrix to hold these
   S_f_model = numpy.matrix(numpy.zeros((3,3), dtype=float))

   #See notes of 9 July 2010
   vt0_var = vtx_covar_matrix[0,0]
   vt1_var = vtx_covar_matrix[1,1]
   vt2_var = vtx_covar_matrix[2,2]
   vt0_vt1_covar = vtx_covar_matrix[0,1]
   vt0_vt2_covar = vtx_covar_matrix[0,2]
   vt1_vt2_covar = vtx_covar_matrix[1,2]
   #Changed, see notes of 7 July 2010
   #and notes of 9 July 2010 to see how these were derived now that there
   #are fractional correlations between the fallspeed errors
   Delta1_var = vt0_var + vt1_var - 2*vt0_vt1_covar
   Delta2_var = vt0_var + vt2_var - 2*vt0_vt2_covar

   vt0_Delta1_covar = vt0_var - vt0_vt1_covar
   vt0_Delta2_covar = vt0_var - vt0_vt2_covar

   S_f_model[0,0] = vt0_var
   S_f_model[1,1] = Delta1_var
   S_f_model[2,2] = Delta2_var
   S_f_model[0,1] = vt0_Delta1_covar
   S_f_model[1,0] = vt0_Delta1_covar
   S_f_model[0,2] = vt0_Delta2_covar
   S_f_model[2,0] = vt0_Delta2_covar
   S_f_model[1,2] = vt0_var - vt0_vt1_covar - vt0_vt2_covar + vt1_vt2_covar
   S_f_model[2,1] = vt0_var - vt0_vt1_covar - vt0_vt2_covar + vt1_vt2_covar
  

   aves = (vt0, Delta1, Delta2)
   return (aves, S_f_model, dV_stats_dB, dVstats_dvtx)


if __name__ == "__main__":
   def mytest():
      (delta0, C0, a0, b0) = fallspeed_forward_model.get_parameters()
      D_major_mm = numpy.zeros((131,), dtype=float)
      D_major_mm[0] = 0.25
      for i_size in range(1, len(D_major_mm)):
        D_major_mm[i_size] = D_major_mm[i_size-1]+0.25
      widths_mm = numpy.ones((131,), dtype=float)*0.25

      N0_est = 2.0e3
      lambda_est = 1.0
      N_D_mm = N0_est*numpy.exp(-lambda_est*D_major_mm)
    
      a_coeff = 0.005
      b_coeff = 2.0
      gamma_coeff = 0.3
      sigma_coeff = 1.88
      f_coeff = 1.0

      myparams = numpy.asmatrix(numpy.zeros((5,1), dtype=float))
      myparams[0,0] = a_coeff
      myparams[1,0] = b_coeff
      myparams[2,0] = gamma_coeff
      myparams[3,0] = sigma_coeff
      myparams[4,0] = f_coeff

      f_radar = 9.35
      K_sq_ice_radar = 0.177
      K_sq_water_radar = 0.93

      T_K = 253.
      P_hPa = 1000.

      obs_vtx_ms = fallspeed_forward_model.fallspeed(D_major_mm, \
                                                     a_coeff,        \
                                                     b_coeff,        \
                                                     gamma_coeff,    \
                                                     sigma_coeff,    \
                                                     T_K,            \
                                                     P_hPa,          \
                                                     delta0,         \
                                                     C0,             \
                                                     a0,             \
                                                     b0)
      print "obs_vtx_ms:"
      print obs_vtx_ms

      obs_vtx_sigmas_ms = obs_vtx_ms*0.5
      (Y_f,S_f) = f_model1(myparams,          \
                     N_D_mm,            \
                     D_major_mm,        \
                     widths_mm,         \
                     T_K,               \
                     P_hPa,             \
                     f_radar,           \
                     K_sq_ice_radar,    \
                     K_sq_water_radar,  \
                     lambda_est)

      print "X at start:" , myparams[0,0], myparams[1,0], myparams[2,0], myparams[3,0], myparams[4,0]
      print "Y_f at start (synthetic observations):", Y_f[0,0], Y_f[1,0],  Y_f[2,0], Y_f[3,0], Y_f[4,0]
      dY_f_dXi = fmodel1_jacobian( myparams,          \
                                   N_D_mm,            \
                                   D_major_mm,        \
                                   widths_mm,         \
                                   obs_vtx_ms,        \
                                   obs_vtx_sigmas_ms, \
                                   T_K,               \
                                   P_hPa,             \
                                   f_radar,           \
                                   K_sq_ice_radar,    \
                                   K_sq_water_radar,  \
                                   lambda_est)

      #Try a retrieval
      #Observations
      Y = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
      Y[0,0] = Y_f[0,0]
      Y[1,0] = Y_f[1,0]
      Y[2,0] = Y_f[2,0]
      Y[3,0] = Y_f[3,0]
      Y[4,0] = Y_f[4,0]

      #Observation and forward model uncertainty
      Se = numpy.asmatrix(numpy.zeros((5,5),dtype=float))
      Se[0,0] = 3.0*3.0
      Se[1,1] = 0.2*0.2
      Se[2,2] = 0.05*0.05
      Se[3,3] = 0.05*0.05
      Se[4,4] = 0.05*0.05

      #Set up retrieval a priori      
      Xa = numpy.asmatrix(numpy.zeros((5,1),dtype=float))
      Xa[0] = 0.01
      Xa[1] = 2.2
      Xa[2] = 0.3
      Xa[3] = 2.0
      Xa[4] = 1.0
      Sa = numpy.asmatrix(numpy.zeros((5,5),dtype=float))
      Sa[0,0] = 0.1*0.1
      Sa[1,1] = 1.1*1.1
      Sa[2,2] = 0.1*0.1
      Sa[3,3] = 3.0*3.0
      Sa[4,4] = 0.5*0.5
       
      (Y_ap,S_ap) = f_model1(Xa,          \
                     N_D_mm,            \
                     D_major_mm,        \
                     widths_mm,         \
                     T_K,               \
                     P_hPa,             \
                     f_radar,           \
                     K_sq_ice_radar,    \
                     K_sq_water_radar,  \
                     lambda_est)

      print "Xa:" , myparams[0,0], myparams[1,0], myparams[2,0], myparams[3,0]
      print "Y_ap:", Y_f[0,0], Y_f[1,0],  Y_f[2,0]


      results = massfit1_oe(Y,                  \
                            Se,                 \
                            Xa,                 \
                            Sa,                 \
                            N_D_mm,             \
                            D_major_mm,         \
                            widths_mm,          \
                            obs_vtx_ms,         \
                            obs_vtx_sigmas_ms,  \
                            T_K,                \
                            P_hPa,              \
                            f_radar,            \
                            K_sq_ice_radar,     \
                            K_sq_water_radar,   \
                            lambda_est,          \
                            diagnostics = True)

      X_fitted = results[0]
      chi_sq = results[1]
      status = results[2]
      A_matrix = results[3]
      print "X_fitted:  "
      print X_fitted
      print "A_matrix:   "
      print A_matrix
      #print A_matrix.trace()
      
      #Evaluate the forward model
      (Y_fitted, S_fitted) = f_model1(X_fitted,           \
                          N_D_mm,             \
                          D_major_mm,         \
                          widths_mm,          \
                          T_K,                \
                          P_hPa,              \
                          f_radar,            \
                          K_sq_ice_radar,     \
                          K_sq_water_radar,   \
                          lambda_est)

      print "Y_fitted:   "
      print Y_fitted


      est_vtx_ms_subset = fallspeed_forward_model.fallspeed(D_major_mm, \
                                                            X_fitted[0,0],  \
                                                            X_fitted[1,0],  \
                                                            X_fitted[2,0],  \
                                                            X_fitted[3,0],  \
                                                            T_K,            \
                                                            P_hPa,          \
                                                            delta0,         \
                                                            C0,             \
                                                            a0,             \
                                                            b0) 

      for i_size in range(len(D_major_mm)):
          print "%15.5e %15.5e %15.5e" %(D_major_mm[i_size], est_vtx_ms_subset[i_size], obs_vtx_ms[i_size])


   def mytest_fmodel():
      print fallspeed_forward_model.initialized
      Xi = numpy.asarray(numpy.zeros((5,1), dtype=float))
      #Xi[0,0] = 0.00348925
      Xi[0,0] = -5.658
      Xi[1,0] = 2.09026
      #Xi[2,0] = 0.284172
      Xi[2,0] = -1.2582
      Xi[3,0] = 1.98306
      Xi[4,0] = 0.85

      #Regime E
      N0_obs_mm = 10.**4.43
      lambda_obs_mm = 1.017
      #a priori
      Xi[0,0] = -6.181
      Xi[1,0] = 2.067
      Xi[2,0] = -1.556
      Xi[3,0] = 1.785
      Xi[4,0] = 0.825

      #retrieved
      Xi[0,0] = -6.396
      Xi[1,0] = 2.14
      Xi[2,0] = -1.673
      Xi[3,0] = 1.976
      Xi[4,0] = 0.853
     



      T_K = 255.
      P_hPa = 1000.
      freq = 9.35
      K_sq_ice = 0.177
      K_sq_water = 0.93

      D_obs_mm = numpy.linspace(0., 26., 105)
      D_obs_mm_uncert = numpy.zeros((105,), dtype=float)
      widths_obs_mm = numpy.ones((105,), dtype=float)*0.25


      N_D_obs_mm = N0_obs_mm*numpy.exp(-lambda_obs_mm*D_obs_mm)
      N_D_obs_mm_uncert = numpy.copy(N_D_obs_mm)
      N_D_obs_mm_uncert[:] = 0.
 
      (Y_f, S_f) = f_model1(Xi, N_D_obs_mm, N_D_obs_mm_uncert, D_obs_mm, D_obs_mm_uncert, widths_obs_mm, T_K, 0., P_hPa, 0., freq, K_sq_ice, 0., K_sq_water, 0.)
      print Y_f

      Xi[4,0] = 0.85+0.0085

      (Y_f_new, S_f_new) = f_model1(Xi, N_D_obs_mm, N_D_obs_mm_uncert, D_obs_mm, D_obs_mm_uncert, widths_obs_mm, T_K, 0., P_hPa, 0., freq, K_sq_ice, 0., K_sq_water, 0.)
      print Y_f[0,0] , Y_f_new[0,0], (Y_f_new[0,0] - Y_f[0,0])/0.0085

   #mytest()
   mytest_fmodel()
