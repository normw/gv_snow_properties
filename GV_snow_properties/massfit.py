#!/usr/bin/env python

"""The main fitting routine for the GV_snow_properties retrieval
"""

#--------------------------------------------------------------------------
# The main fitting routine for the GV_snow_properties retrieval
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


#This is the main fitting routine

import numpy
import forward_models_oe


def massfit1_oe(Y,                           \
                Se,                          \
                Xa,                          \
                Sa,                          \
                N_D_mm_obs,                  \
                N_D_mm_obs_uncert,           \
                D_major_mm_obs,              \
                D_major_mm_obs_uncert,       \
                widths_mm_obs,               \
                T_K_obs,                     \
                T_uncert_K_obs,              \
                P_hPa_obs,                   \
                P_uncert_hPa_obs,            \
                radar_obs_freq,              \
                radar_obs_K_sq_ice,          \
                radar_obs_K_sq_ice_uncert,   \
                radar_obs_K_sq_water,        \
                radar_obs_K_sq_water_uncert, \
                diagnostics = False):


   MAX_ITERATIONS = 30
   Se_current = numpy.copy(Se)
   #See notes about this change in parameter_eval_massfit1_oe.py, case25i
   Xi = numpy.copy(Xa)
   #Xi[:,0] = [-5.64590e+00, 2.08706e+00, -7.33377e-01, 1.80928e+00, 8.18383e-01]

   
   n_iterations = 0
   d2_test = 1000.

   status = 0
   #while d2_test > 3. and n_iterations < 100:
   #while d2_test > .3 and n_iterations < MAX_ITERATIONS:
   while d2_test > 0.01*5 and n_iterations < MAX_ITERATIONS:
     #Evaluate the forward model and its jacobian at xi
     (Y_f, S_f) = forward_models_oe.fmodel1(Xi,                  \
                    N_D_mm_obs,                 \
                    N_D_mm_obs_uncert,          \
                    D_major_mm_obs,             \
                    D_major_mm_obs_uncert,      \
                    widths_mm_obs,              \
                    T_K_obs,                    \
                    T_uncert_K_obs,             \
                    P_hPa_obs,                  \
                    P_uncert_hPa_obs,           \
                    radar_obs_freq,             \
                    radar_obs_K_sq_ice,         \
                    radar_obs_K_sq_ice_uncert,  \
                    radar_obs_K_sq_water,       \
                    radar_obs_K_sq_water_uncert)

     #print "In massfit1, iteration %d" %(n_iterations)
     #print "Xi:  ", Xi
     #print "Y_fi: ", Y_f

     
     K_f = forward_models_oe.fmodel1_jacobian(Xi,                        \
                            N_D_mm_obs,                \
                            N_D_mm_obs_uncert,         \
                            D_major_mm_obs,            \
                            D_major_mm_obs_uncert,     \
                            widths_mm_obs,             \
                            T_K_obs,                   \
                            T_uncert_K_obs,            \
                            P_hPa_obs,                 \
                            P_uncert_hPa_obs,          \
                            radar_obs_freq,            \
                            radar_obs_K_sq_ice,        \
                            radar_obs_K_sq_ice_uncert, \
                            radar_obs_K_sq_water,      \
                            radar_obs_K_sq_water_uncert)

     Se_current = Se + S_f


     #Incrementing scheme, n-form (Rodgers (2008), eq. 5.9)
     Sa_inv = numpy.linalg.pinv(Sa)
     try:
        Se_current_inv = numpy.linalg.pinv(Se_current)
     except:
        print Se_current
        assert 0
     termA = (Sa_inv +K_f.T*Se_current_inv*K_f)
     termA_inv = numpy.linalg.pinv(termA)
     termB = K_f.T*Se_current_inv*(Y - Y_f + K_f*(Xi - Xa))
     step_scale = numpy.linalg.norm(termA_inv,2)
     step_size = step_scale
     X_n = Xa + step_size/step_scale*termA_inv*termB

     #The first element of X_n is now ln(alpha), so may be negative
     #Change the check of X_n < 0 to X_n[1,0] < 0 or X_n[3,0] < 0 or X_n[4,0] < 0

     #Ditto for the third element of X_n, which is now ln(gamma), so may be negative
     n_step_iterations = 0
     #Need a different form of B for this iteration, see notes from 29 Dec 2010
     termBprime = K_f.T*Se_current_inv*(Y - Y_f) + Sa_inv*(Xi - Xa)
     #while numpy.any(X_n < 0., axis=0) and n_step_iterations < 10:
     while (X_n[1,0] < 0 or X_n[3,0] < 0 or X_n[4,0] < 0) and n_step_iterations < 10:
        step_size = step_size/2.

        ######## I'm not sure this is right!!!!!! #######
        ######## Shouldn't X_n be incremented from X_a per Rodgers (2008), eq. 5.9?)
        ######## e.g. X_n = Xa + step_size/step_scale*termA_inv*termB
        ######## Compare Rogers (2008) equations 5.8 and 5.9
        ######## Fixed, use termBprime from above
        #X_n = Xi + step_size/step_scale*termA_inv*termB
        X_n = Xi + step_size/step_scale*termA_inv*termBprime
        n_step_iterations += 1

     if n_step_iterations >= 10.:
        X_final = numpy.copy(X_n)
        print "Exceeding step size reduction iterations"
        break
     

     #Test for convergence based on delta_x (see Rodgers (2008), equations 5.29 and 5.30)
     delta_X = Xi - X_n
     Sx_inv = Sa.I + K_f.T*Se_current.I*K_f
     Sx = Sx_inv.I
     d2_test = delta_X.T*Sx_inv*delta_X

     n_iterations += 1
     Xi = X_n


   #Iteration done
   X_final = numpy.copy(X_n)
   test_finite = numpy.all(numpy.isfinite(X_final))
   #Need to modify this test, since X_final[0,0] now contains ln(alpha) and may be negative
   #Ditto for X_final[2,0], which contains ln(gamma) now
   #test_positive = numpy.all(X_final > 0)
   test_positive = X_final[1,0] > 0 and X_final[3,0] > 0 and X_final[4,0] > 0
   #print "#massfit1:  N_ITERATIONS: %8d" %(n_iterations,)

   if n_iterations < MAX_ITERATIONS and test_finite == True and test_positive == True and status == 0:
     #Evaluate the forward model and its jacobian at X_final
      (Y_f_final, S_f_final) = forward_models_oe.fmodel1(X_final,             \
                                 N_D_mm_obs,                 \
                                 N_D_mm_obs_uncert,          \
                                 D_major_mm_obs,             \
                                 D_major_mm_obs_uncert,      \
                                 widths_mm_obs,              \
                                 T_K_obs,                    \
                                 T_uncert_K_obs,             \
                                 P_hPa_obs,                  \
                                 P_uncert_hPa_obs,           \
                                 radar_obs_freq,             \
                                 radar_obs_K_sq_ice,         \
                                 radar_obs_K_sq_ice_uncert,  \
                                 radar_obs_K_sq_water,       \
                                 radar_obs_K_sq_water_uncert)

      
      Se_final = Se + S_f_final
      #Se_inv = numpy.linalg.pinv(Se_current)
      #Sa_inv = numpy.linalg.pinv(Sa)
      #Compute the chi-sq statistic
      #See discussion in Rodgers (2008) section 5.6.4 and equation 5.32
      #Alternate form for chi-square - see Marks & Rodgers, 1993 equations 16 and 17 and related discussion
      chi_sq = (Y - Y_f_final).T*Se_final.I*(Y - Y_f_final) + (Xa - X_final).T*Sa.I*(Xa - X_final)
      if diagnostics == True:
         #Get the Jacobian at X_final
         K_f_final = forward_models_oe.fmodel1_jacobian(X_final,                    \
                                      N_D_mm_obs,                 \
                                      N_D_mm_obs_uncert,          \
                                      D_major_mm_obs,             \
                                      D_major_mm_obs_uncert,      \
                                      widths_mm_obs,              \
                                      T_K_obs,                    \
                                      T_uncert_K_obs,             \
                                      P_hPa_obs,                  \
                                      P_uncert_hPa_obs,           \
                                      radar_obs_freq,             \
                                      radar_obs_K_sq_ice,         \
                                      radar_obs_K_sq_ice_uncert,  \
                                      radar_obs_K_sq_water,       \
                                      radar_obs_K_sq_water_uncert)

         #Compute the final A-matrix
         Se_condition = numpy.linalg.cond(Se_final)
         #Sa_inv = numpy.linalg.pinv(Sa)
         #Se_inv = numpy.linalg.pinv(Se_current)

         A_term1 = K_f_final.T*Se_final.I*K_f_final + Sa.I
         #A_term1_inv = numpy.linalg.pinv(A_term1)
         try:
            G_matrix = A_term1.I*K_f_final.T*Se_final.I
            A_matrix = A_term1.I*K_f_final.T*Se_final.I*K_f_final
         except:
            print K_f_final
            assert 0
         cov_matrix = A_term1.I

         #Write out the A matrix and do some other diagnostics
         f_A_matrix_out = open("A_matrix.txt","a")
         In = numpy.asmatrix(numpy.zeros((5,5), dtype=float))
         In[0,0] = 1.
         In[1,1] = 1.
         In[2,2] = 1.
         In[3,3] = 1.
         In[4,4] = 1.
         
         X_inv = numpy.copy(In)
         X_inv[0,0] = 1./X_final[0,0]
         X_inv[1,1] = 1./X_final[1,0]
         X_inv[2,2] = 1./X_final[2,0]
         X_inv[3,3] = 1./X_final[3,0]
         X_inv[4,4] = 1./X_final[4,0]

         #frac_ap = ((In - A_matrix)*Xa).T*X_inv
         frac_ap = X_inv*(In - A_matrix)*Xa

         for i in range(5):
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(A_matrix[i,j]))
            f_A_matrix_out.write("\n")
         #f_A_matrix_out.write("#%15.5e %15.5e %15.5e %15.5e %15.5e\n" %(frac_ap[0,0], frac_ap[0,1], frac_ap[0,2], frac_ap[0,3], frac_ap[0,4]))
         f_A_matrix_out.write("#%15.5e %15.5e %15.5e %15.5e %15.5e\n" %(frac_ap[0,0], frac_ap[1,0], frac_ap[2,0], frac_ap[3,0], frac_ap[4,0]))

         #Extra diagnostics
         #Write some extra diagnostics to A_matrix.txt
         tmp = K_f_final
         f_A_matrix_out.write("##K_f_final:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = Se_final
         f_A_matrix_out.write("##Se:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = Se_final.I
         f_A_matrix_out.write("##Se_inverse:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = numpy.linalg.pinv(Se_final)
         f_A_matrix_out.write("##Se_inverse_alt:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = Sa
         f_A_matrix_out.write("##Sa:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = Sa.I
         f_A_matrix_out.write("##Sa_inverse:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = A_term1.I
         f_A_matrix_out.write("##A_term1_inverse:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
         tmp = numpy.linalg.pinv(A_term1)
         f_A_matrix_out.write("##A_term1_inverse_alt:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
            
         tmp = G_matrix
         f_A_matrix_out.write("##G_matrix:\n")
         for i in range(5):
            f_A_matrix_out.write("##")
            for j in range(5):
               f_A_matrix_out.write("%15.5e" %(tmp[i,j]))
            f_A_matrix_out.write("\n")
            
         f_A_matrix_out.close()
         #End temporary diagnostics

         return(X_final, chi_sq, status, A_matrix, cov_matrix)
      else:
         return(X_final, chi_sq, status)

   else:
      #Some sort of unsuccessful retrieval
      chi_sq = -999.
      X_final[0,0] = -999.
      X_final[1,0] = -999.
      X_final[2,0] = -999.
      X_final[3,0] = -999. 
      X_final[4,0] = -999.
      if n_iterations >= MAX_ITERATIONS:      
         #Retrieval failed
         print "#massfit1_oe:  Too many iterations"
         status = 1
      elif test_finite == False:
         print "#massfit1_oe:  Non-finite result"
         #Retrieval failed
         status = 2
      #Need to change this test, since now X_final[0,0] is ln(alpha) and will be negative
      #elif X_final[0,0] < 0. or X_final[2,0] < 0.:
      elif X_final[1,0] < 0. or X_final[2,0] < 0. or X_final[3,0] < 0. or X_final[4,0] < 0.:
         print "#massfit1_oe:  Negative coefficients retrieved"
         print X_final
         status = 3
      elif status == 5:
         print "#massfit1_oe:  SVD failed to converge"
      else:
         print "#massfit1_oe:  Other failed retrieval"
         status = 6

      if diagnostics == True:
         A_matrix = numpy.ones((4,4), dtype=float)*(-999.)
         cov_matrix = numpy.ones((4,4), dtype=float)*(-999)
         return(X_final, chi_sq, status, A_matrix, cov_matrix)
      else:
         return(X_final, chi_sq, status)

    

