#!/usr/bin/env /usr/bin/python


"""Radar (Rayleigh-regime) forward model
"""

#--------------------------------------------------------------------------
# Radar (Rayleigh-regime) forward model
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import numpy, scipy.integrate
import particle_properties
SPEED_OF_LIGHT = 2.99792e8   # m s^-1
RHO_ICE = 917.0             #kg m^-3


def sigma_bk_rayleigh(D_ev, waveln, K_sq):
   #D_ev in m, waveln in m
   sigma_bk = numpy.power(D_ev,6)*K_sq*numpy.power(numpy.pi,5)/(numpy.power(waveln,4))  #m^2
   return sigma_bk

def rayleigh_sim_spectrum2(psd_data,    \
                           D_major_mm,  \
                           delta_D,     \
                           alpha,       \
                           beta,        \
                           vtx,         \
                           K_sq_water,  \
                           K_sq_ice,    \
                           diagnostics = False):
   #Expects:
   #  psd_data in m^-3 mm^-1, size (N_bins),
   #  D_major in mm, size (N_bins),
   #  alpha, beta - coefficients of power law m(D)
   #  vtx in m/s
   #  f_radar in GHz, scalar
   #  K_sq_water, scalar, should match frequency f_radar
   #  K_sq_ice, scalar, should match frequency f_radar
   
   #Evaluate the particle properties at the bin sizes
   
   
   #Build an array of sigma_bk that match psd_data in dimension
   N_bins = len(psd_data)
   masses_kg = particle_properties.mass_pow(D_major_mm*1.0e-3, alpha, beta)
   D_ev = numpy.power(6.*masses_kg/(RHO_ICE*numpy.pi),1./3.)     #In meters, reduce to solid ice spheres
   delta_D_major_mm = 0.01*D_major_mm[1]                         #Use 1% of the smallest nonzero D_major_mm
   D_major_pert_mm = D_major_mm + delta_D_major_mm
   masses_pert_kg = particle_properties.mass_pow(D_major_pert_mm*1.0e-3, alpha, beta)
   D_ev_pert = numpy.power(6.*masses_pert_kg/(RHO_ICE*numpy.pi),1./3.)
   D_ev_6th = numpy.power(D_ev,6.)
   D_ev_pert_6th = numpy.power(D_ev_pert, 6.)
   delta_psd = psd_data*0.01                                     #The magnitudes of PSD values can vary considerably
                                                                 #so use 1% of the PSD value where the PSD value is non
                                                                 #zero, and use 1% of the smallest nonzero PSD value where
                                                                 #the PSD value is zero
   indices = numpy.where(numpy.greater(delta_psd, 0.))[0]
   min_delta_psd = numpy.min(delta_psd[indices])
   indices = numpy.where(numpy.less_equal(delta_psd,0.))[0]
   delta_psd[indices] = min_delta_psd
   psd_data_pert = psd_data + delta_psd
   
   eta_int = numpy.zeros((N_bins,), dtype=float)
   eta_vtx_int = numpy.zeros((N_bins,), dtype=float)
   eta_int_pert_D = numpy.zeros((N_bins,), dtype=float)
   eta_int_pert_N = numpy.zeros((N_bins,), dtype=float)

   eta_int[0] = D_ev_6th[0]*psd_data[0]*delta_D[0]/2.
   eta_int[1:-1] = D_ev_6th[1:-1]*psd_data[1:-1]*delta_D[1:-1]
   eta_int[-1] = D_ev_6th[-1]*psd_data[-1]*delta_D[-1]/2.

   eta_vtx_int[0] = D_ev_6th[0]*psd_data[0]*delta_D[0]*vtx[0]/2.
   eta_vtx_int[1:-1] = D_ev_6th[1:-1]*psd_data[1:-1]*delta_D[1:-1]*vtx[1:-1]
   eta_vtx_int[-1] = D_ev_6th[-1]*psd_data[-1]*delta_D[-1]*vtx[-1]/2.

   eta_int_pert_D[0] = D_ev_pert_6th[0]*psd_data[0]*delta_D[0]/2.
   eta_int_pert_D[1:-1] = D_ev_pert_6th[1:-1]*psd_data[1:-1]*delta_D[1:-1]
   eta_int_pert_D[-1] = D_ev_pert_6th[-1]*psd_data[-1]*delta_D[-1]/2.

   eta_int_pert_N[0] = D_ev_6th[0]*psd_data_pert[0]*delta_D[0]/2.
   eta_int_pert_N[1:-1] = D_ev_6th[1:-1]*psd_data_pert[1:-1]*delta_D[1:-1]
   eta_int_pert_N[-1] = D_ev_6th[-1]*psd_data_pert[-1]*delta_D[-1]


   eta_int_sum = numpy.sum(eta_int)
   eta_vtx_int_sum = numpy.sum(eta_vtx_int)

   Z_rayl = K_sq_ice/K_sq_water*numpy.sum(eta_int)*1.0e18        #The sum of eta_int is in m^6 m^-3, need to convert
                                                                 #to mm^6 m^-3, so multiply by 1.0e3^6 = 1.0e18
   dBZe_rayl = 10.*numpy.log10(Z_rayl)

   refl_wtd_fallspeed = eta_vtx_int_sum/eta_int_sum
 
   
   Z_rayl_pert = numpy.zeros((2*N_bins+2),dtype=float)
   #Calculate eta_pert due to perturbations of D and N(D) for each size bin
   #Store eta_pert in Z_rayl_pert
   for i_bin in range(0,N_bins):
      Z_rayl_pert[i_bin] = eta_int_sum - eta_int[i_bin] + eta_int_pert_D[i_bin]
      Z_rayl_pert[i_bin+N_bins] = eta_int_sum - eta_int[i_bin] + eta_int_pert_N[i_bin]
   
   #Now convert eta_pert to Z_rayl_pert
   Z_rayl_pert = Z_rayl_pert*K_sq_ice/K_sq_water*1.0e18

   #Now calculate the Z_rayl_pert due to perturbations in K_sq_ice and K_sq_water
   Z_rayl_pert[2*N_bins] = Z_rayl/(K_sq_ice)*(K_sq_ice*1.01)              #Very explicit, just to help me remember
   Z_rayl_pert[2*N_bins+1] = Z_rayl*K_sq_water/(K_sq_water*1.01)          #Ditto

   delta_dBZe = 10.*numpy.log10(Z_rayl_pert) - dBZe_rayl

   d_dBZe_dB = numpy.zeros((2*N_bins+2), dtype=float)
   d_dBZe_dB[0:N_bins] = delta_dBZe[0:N_bins]/delta_D_major_mm                #Sensitivity to D
   d_dBZe_dB[N_bins:2*N_bins] = delta_dBZe[N_bins:2*N_bins]/delta_psd         #Sensitivity to PSD

   d_dBZe_dB[2*N_bins] = delta_dBZe[2*N_bins]/(0.01*K_sq_ice)
   d_dBZe_dB[2*N_bins+1] = delta_dBZe[2*N_bins+1]/(0.01*K_sq_water)

   return(dBZe_rayl, refl_wtd_fallspeed, d_dBZe_dB)


if __name__ == '__main__':

   def mytest2():
      import MH_2005
      r_evs = numpy.array([   5.0,   15.0,   30.0,   45.0,   60.0,   75.0,   90.0,  105.0,  120.0,  150.0,  180.0,
             210.0,  240.0,  270.0,  290.0,  310.0,  340.0,  370.0,  400.0,  420.0,  490.0,  555.0,
             610.0,  670.0,  720.0,  770.0,  815.0,  860.0,  900.0,  940.0,  985.0, 1020.0, 1060.0,
            1095.0, 1130.0, 1170.0, 1200.0, 1265.0, 1300.0, 1330.0, 1365.0, 1390.0, 1420.0, 1455.0 ], dtype=float)
      r_evs = r_evs*25.           #This choice of sizes and frequency should produce a figure that
                                  #matches Stephens(1994) Figure 5.29a (_Remote Sensing of the Lower
                                  #Atmosphere_).  I've verified that the backscatter efficiencies do match.
      #Try computing integrated properties using both rayleigh_sim_spectrum and rayleigh_sim_spectrum2
      r_evs = r_evs/25.
      f_radar = 9.3
      waveln_m = SPEED_OF_LIGHT/f_radar*1.0e-9  # m
      K_sq_ice = 0.177
      K_sq_water = 0.93
      T_K = 263.
      P_hPa = 1000.
      a_coeff = 0.003489
      b_coeff = 2.090
      gamma_coeff = 0.35
      sigma_coeff = 1.9
      #Convert r_evs to D_major_mm
      rho_ice_cgs = RHO_ICE/1000.
      D_evs_cm = r_evs*2.0e-4
      D_evs_m = r_evs*2.0e-6
      D_major_mm = numpy.power((rho_ice_cgs*numpy.pi*numpy.power(D_evs_cm,3.)/(6.*a_coeff)),1./b_coeff)*10.
      delta_D_list = []
      for i_size in range(1,len(D_major_mm)):
         delta_D_val = D_major_mm[i_size] - D_major_mm[i_size-1]
         delta_D_list.append(delta_D_val)
      delta_D_list.append(delta_D_list[-1])
      delta_D_mm = numpy.array(delta_D_list, dtype=float)

      N0_mm = 4896
      lambda_mm = 1.567
      N_D_mm = N0_mm*numpy.exp(-lambda_mm*D_major_mm)

      #Get the fallspeeds
      (delta0, C0, a0, b0) = MH_2005.get_parameters()
      vtx_local = MH_2005.fallspeed(D_major_mm,    \
                                    a_coeff,       \
                                    b_coeff,       \
                                    gamma_coeff,   \
                                    sigma_coeff,   \
                                    T_K,           \
                                    P_hPa,         \
                                    delta0,        \
                                    C0,            \
                                    a0,            \
                                    b0)

      #Construct the variances in D_major_mm and N_D_mm
      N_bins = len(r_evs)
      var_D_major_mm = numpy.power(D_major_mm*.05,2)
      var_N_D_mm = numpy.power(N_D_mm*.15,2)
      var_K_sq_water = numpy.power(K_sq_water*0.001,2)
      var_K_sq_ice = numpy.power(K_sq_ice*0.001,2)
      (dBZe, refl_wtd_fallspeed, ddBZe_dB) = rayleigh_sim_spectrum2(N_D_mm, D_major_mm, delta_D_mm, a_coeff, b_coeff, vtx_local, K_sq_water, K_sq_ice)
      #Calculate an uncertainty
      var_dBZe = numpy.zeros(len(ddBZe_dB), dtype=float)
      var_dBZe[0:N_bins] = ddBZe_dB[0:N_bins]*var_D_major_mm*ddBZe_dB[0:N_bins]
      var_dBZe[N_bins:2*N_bins] = ddBZe_dB[N_bins:2*N_bins]*var_N_D_mm*ddBZe_dB[N_bins:2*N_bins]
      var_dBZe[2*N_bins] = ddBZe_dB[2*N_bins]*var_K_sq_ice*ddBZe_dB[2*N_bins]
      var_dBZe[2*N_bins+1] = ddBZe_dB[2*N_bins+1]*var_K_sq_water*ddBZe_dB[2*N_bins+1]
      sigma_dBZe = numpy.sqrt(numpy.sum(var_dBZe))

      print dBZe, sigma_dBZe, refl_wtd_fallspeed

   #mytest1()
   mytest2()
   #mytest3()
