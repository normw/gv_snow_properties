#!/usr/bin/env python

"""Parser for retrieval configuration
"""

#--------------------------------------------------------------------------
# Parser for retrieval configuration
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import ConfigParser

#Parse the local config file info

config = ConfigParser.RawConfigParser()
config.read('config.cfg')


#assert 0
#event = config.get('Case', 'event')
#location = config.get('Case', 'location')
#mrr_instrument = config.get('Case', 'MRR_instrument')
#pip_instrument = config.get('Case', 'PIP_instrument')

