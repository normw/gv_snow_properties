#!/usr/bin/env python


"""Structure and methods for radar data
"""

#--------------------------------------------------------------------------
# Structure and methods for radar data
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import collections
import numpy
import netCDF4

#EM properties for different radar's frequencies

EMData = collections.namedtuple('EMData', ['freq',
                                           'K_sq_ice',
                                           'K_sq_water',
                                           'K_sq_ice_uncert',
                                           'K_sq_water_uncert',
                                           'Tice_ref',
                                           'Tliq_ref'])

RadarData = collections.namedtuple('RadarData', ['times_utc',
                                                 'valid',
                                                 'Ze',
                                                 'Vdop'])

em_dict = {'VERTIX': EMData(freq = 9.35,
                            K_sq_ice = 0.177,
                            K_sq_water = 0.930,
                            K_sq_ice_uncert = 0.001,
                            K_sq_water_uncert = 0.,
                            Tice_ref = 250.,
                            Tliq_ref = 273.),
           'MRR':    EMData(freq = 24.0,
                            K_sq_ice = 0.177,
                            K_sq_water = 0.908,
                            K_sq_ice_uncert = 0.001,
                            K_sq_water_uncert = 0.,
                            Tice_ref = 250.,
                            Tliq_ref = 273.)}


def near(x, y):
    atol = 1.e-08
    rtol = 1.e-05
    mask = numpy.less_equal(numpy.abs(x-y), atol + rtol*numpy.abs(y))
    return mask

def init(radar_fpath):
    IDX_NS_BIN = 3

    f_ptr = netCDF4.Dataset(radar_fpath, 'r')

    times_utc = numpy.array(f_ptr['UTC_start'][:] + f_ptr['time_offset'][:], dtype='datetime64[s]')

    reflectivity = f_ptr['Reflectivity'][:]
    #reflectivity_missing_value = f_ptr['Reflectivity'].missing_value
    reflectivity_missing_value = -999.
    doppler_velocity = f_ptr['Doppler_velocity'][:]

    Ze = reflectivity[:, IDX_NS_BIN]
    Vdop = doppler_velocity[:, IDX_NS_BIN]
    invalid_mask = near(Ze, reflectivity_missing_value)
    valid = numpy.logical_not(invalid_mask)

    data = RadarData(times_utc = times_utc,
                     valid = valid,
                     Ze = Ze,
                     Vdop = Vdop)

    return data




if __name__ == '__main__':
    radar_fpath = '/boltzmann/data6/norm/GV_precip/GCPEx/Case_composites/05_min/CARE/mrr_E02_CARE_NASA_05_minute.cdf'

    data = init(radar_fpath)
    print data.valid
    print data.Ze
    print data.Vdop


  














#print em_dict['MRR']

