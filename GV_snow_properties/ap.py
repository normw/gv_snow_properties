#!/usr/bin/env python

"""
A priori expected values and uncertainties
"""

#--------------------------------------------------------------------------
# A priori expected values and uncertainties
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------

import numpy

#A priori settings

Xa = numpy.asmatrix(numpy.zeros((5, 1), dtype=float))
Sa = numpy.asmatrix(numpy.zeros((5, 5), dtype=float))

#Final runs, I think.
#Set the a priori alpha, beta, gamma, sigma to current values
#Set the a priori phi to 0.825, based on my modeled phi values appropriate for
#Korolev & Isaac's(2003) typical aspect ratios (ratio of Dmax divided _into_
#D perpindicular to Dmax, similar to b/a in my evaluations of Dobs vs Dmax)
#of 0.60 to 0.70, and set a 1-sigma value on phi of 0.1
##Case25f, Case26f
##Using means, variances and covariances from the a priori data analysis
Xa[0,0] = -6.181
Xa[1,0] = 2.067
Xa[2,0] = -1.556
Xa[3,0] = 1.785
Xa[4,0] = 0.825    #Factor applied to rescale D_obs to D_max, based on
                   #Korolev & Isaac 2003 typical aspect ratio of 0.6 to
                   #0.65 especially for larger particles.
Sa[0,0] = 2.474
Sa[0,1] = 0.585
Sa[1,0] = 0.585
Sa[1,1] = 0.244
Sa[2,2] = 0.392
Sa[2,3] = 0.118
Sa[3,2] = 0.118
Sa[3,3] = 0.0507
Sa[4,4] = 0.125*0.125  #Uncertainty in factor applied to rescale D_obs to D_max
