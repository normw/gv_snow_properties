#!/usr/bin/env python


"""Structure and methods for Precipitation Imaging Package data
"""

#--------------------------------------------------------------------------
# Structure and methods for Precipitation Imaging Package data
#
# Copyright (c) 2018 Norman Wood
#
# This file is part of the free software GV_snow_properties:  you can
# redistribute it and/or modify it under the terms of the BSD 3-Clause
# License.
# 
# The software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# BSD 3-Clause License for more details
# 
# You should have received a copy of the BSD 3-Clause License in the file
# LICENSE distributed along with this software.  If not, see
# <https://spdx.org/licenses/BSD-3-Clause.html>.
#--------------------------------------------------------------------------


import collections
import numpy
import netCDF4


#See init() for definitions
_FOV_MAX_WIDTH = None    #mm
_FOV_MAX_HEIGHT = None   #mm
_FPS = None
_DOF_FACTOR = None
_FSI = None


PIPData = collections.namedtuple('PIPData', ['times_utc',
                                             'dt_sampling',
                                             'Di',
                                             's_Di',
                                             'dDi',
                                             'N_Di',
                                             's_N_Di'])

def init(fpath, instrument=None):
    global _FOV_MAX_WIDTH, _FOV_MAX_HEIGHT, _FPS, _DOF_FACTOR, _FSI

    if instrument is None:
        #Use SVI if no instrument specified, maintains backward compatibility
        instrument = 'SVI'

    if instrument == 'SVI' or instrument == 'PVI':
        _FOV_MAX_WIDTH = 32.    #FOV maximum width, mm
        _FOV_MAX_HEIGHT = 24.   #FOV maxium height, mm
        _FPS = 60.              #Frame rate, frames per second
        _DOF_FACTOR = 117.      #Gives DOF in cm when multiplied by
                                #    particle size in mm
        _FSI = 1.               #Frame sampling interval, number of frames skipped
                                #    when smapling for calculating size distribution
    elif instrument == 'PIP':
        _FOV_MAX_WIDTH = 64.
        _FOV_MAX_HEIGHT = 48.
        _FPS = 380.
        _DOF_FACTOR = 117./2.
        _FSI = 25.
    else:
        sys.stderr.write('Unrecognized instrument type in GV_snow_properties.precip_img_pkg.init():  %s\n' %(instrument,))
        sys.exit(1)
        

    data = None
    if fpath is not None:  
        f_ptr = netCDF4.Dataset(fpath, 'r')

        times_utc = numpy.array(f_ptr['UTC_start'][:] + f_ptr['time_offset'][:], dtype='datetime64[s]')
        dt_sampling = f_ptr['sample_interval'][:]
        Di = f_ptr['Di'][:]
        s_Di = f_ptr['Di_uncert'][:]
        N_Di = f_ptr['N_Di_mean'][:]
        s_N_Di = f_ptr['N_Di_uncert'][:]

        N_sizes = Di.shape[0]
        dDi = numpy.zeros((N_sizes,), dtype=Di.dtype)
        dDi[0:N_sizes-1] = numpy.diff(Di)
        dDi[-1] = dDi[-2]

        data = PIPData(times_utc = times_utc,
                       dt_sampling = dt_sampling,
                       Di = Di,
                       s_Di = s_Di,
                       dDi = dDi,
                       N_Di = N_Di,
                       s_N_Di = s_N_Di)

    return data
    

def sample_volumes(Di, delta_t):
    #Di:       bounds on size bins in mm
    #delta_t:  sampling time in s

    _MM_TO_M = 1.0e-3
    _CM_TO_M = 1.0e-2
    fov_width = (_FOV_MAX_WIDTH - Di)*_MM_TO_M
    fov_height = (_FOV_MAX_HEIGHT - Di)*_MM_TO_M
    depth_of_field = _DOF_FACTOR*Di*_CM_TO_M
    volumes = fov_width*fov_height*depth_of_field*_FPS/_FSI*delta_t
    #Handle negative fov widths and heights
    crit_mask = numpy.logical_or(fov_width < 0., fov_height < 0.)
    volumes[crit_mask] = 0.

    return volumes


if __name__ == "__main__":
    pip_fpath = "/boltzmann/data6/norm/GV_precip/GCPEx/Case_composites/05_min/CARE/pip_E02_CARE_102_05_minute.cdf"

    pip_data = init(pip_fpath)
    print pip_data.Di
    print pip_data.N_Di
    delta_t = 300.
    v = sample_volumes(pip_data.Di, delta_t)
